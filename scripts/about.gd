class_name About
extends Control


@onready var margin_top = $VBoxAbout/MarginTop
@onready var margin_board = $VBoxAbout/TextureBoard/MarginBoard
@onready var label_about = margin_board.get_node("PanelBoard/MarginAbout/LabelAbout")
@onready var margin_bottom = $VBoxAbout/MarginBottom
@onready var animation_about = $AnimationAbout


func _on_ready():
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    animation_about.play("fade_in")

    label_about.text = label_about.text.format({
        "game": tr("Memoria").to_upper(),
        "copyright": tr("© 2024 Roma Aeterna"),
        "version": tr("Version"),
        "version_number": ProjectSettings.get_setting("application/config/version"),
        "programming": tr("Programming & Design").to_upper(),
        "textures": tr("Textures & Icons").to_upper(),
        "fonts": tr("Fonts").to_upper(),
        "sounds": tr("Sounds").to_upper(),
        "licenses": tr("Licenses").to_upper(),
        "godot": tr("Made with Godot Engine 4")})


func _on_animation_about_finished(animation_name):
    if animation_name == "fade_out":
        var next_scene = "res://scenes/main.tscn"
        get_tree().change_scene_to_file(next_scene)


func _on_button_close_pressed():
    animation_about.play("fade_out")


func _on_label_about_meta_clicked(meta):
    OS.shell_open(str(meta))
