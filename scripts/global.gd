class_name Global
extends Node


class vocable:
    var unit
    var source
    var target

    func _init(vocable_unit, vocable_source, vocable_target):
        self.unit = vocable_unit
        self.source = vocable_source
        self.target = vocable_target


class player:
    var number
    var name
    var score
    var label_score
    var color

    func _init(player_number, player_color):
        self.number = player_number
        self.name = ""
        self.score = 0
        self.label_score = null
        self.color = player_color


# internal variables
var app_start = true
var documents_dir = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)
var last_file = ""
var last_scene = ""

# network
var host = false
var client_index = 0
var clients = []
var lobby = ""
var peer = null
var timer_mode = 1

# settings
## general
var countdown_length = 5
var language = OS.get_locale_language()
var sounds = 1.0
## vocables
var default_card_number = 24
var default_dir = documents_dir
var default_file = ""

# game setup
var card_number = 0
var high_score = []
var player1 = player.new(1, Color.DARK_BLUE)
var player2 = player.new(2, Color.DARK_RED)
var player3 = player.new(3, Color.DARK_GREEN)
var player4 = player.new(4, Color.ORANGE)
var player_number = 2
var winner = null
var unit_names = []
var unit_vocables = []
var vocables = []


func _init():
    randomize()
    load_settings()


func load_settings():
    var config = ConfigFile.new()
    config.load("user://settings.ini")

    countdown_length = config.get_value("General", "countdown", 5)
    language = config.get_value("General", "language", OS.get_locale_language())
    sounds = config.get_value("General", "sounds", 1.0)
    default_card_number = config.get_value("Vocables", "default_card_number", 24)
    default_dir = config.get_value("Vocables", "default_dir", documents_dir)
    default_file = config.get_value("Vocables", "default_file", "")
    
    TranslationServer.set_locale(language)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(sounds))


func load_high_score():
    var file = FileAccess.open("user://high_score.txt", FileAccess.READ)
    if not file:
        return
    
    var data = file.get_as_text().split("\n")
    high_score = []
    for i in range(len(data) - 1):
        var array = data[i].split(";")
        var score = [array[0], int(array[1])]
        high_score.append(score)
    high_score.sort_custom(func(a, b): return a[1] > b[1])


func update_high_score():
    var new_player = true
    for score in high_score:
        if score[0] == global.winner.name:
            score[1] += 1
            new_player = false
    if new_player:
        high_score.append([global.winner.name, 1])
    high_score.sort_custom(func(a, b): return a[1] > b[1])
    global.winner = null
    save_high_score()


func save_high_score():
    var data = ""
    for score in high_score:
        data += score[0] + ";" + str(score[1]) + "\n"
    
    var file = FileAccess.open("user://high_score.txt", FileAccess.WRITE)
    file.store_string(data)
