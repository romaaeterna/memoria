class_name SetupLocal
extends Control


var current_color_button = null
var java_script_callback = null
var next_scene = ""

var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

@onready var margin_top = $VBoxSetup/MarginTop

@onready var margin_board = $VBoxSetup/TextureBoard/MarginBoard
@onready var hbox_board = margin_board.get_node("HBoxBoard")
@onready var vbox_file = hbox_board.get_node("PanelFile/MarginFile/VBoxFile")
@onready var label_file = vbox_file.get_node("HBoxFile/LabelFile")
@onready var flow_units = vbox_file.get_node("PanelUnits/MarginUnits/ScrollUnits/FlowUnits")
@onready var hbox_number = vbox_file.get_node("VBoxCards/HBoxNumber")

@onready var vbox_players = hbox_board.get_node("PanelPlayers/MarginPlayers/VBoxPlayers")
@onready var panel_player_1 = vbox_players.get_node("PanelPlayer1")
@onready var vbox_player_1 = panel_player_1.get_node("MarginPlayer1/VBoxPlayer1")
@onready var check_player_1 = vbox_player_1.get_node("CheckPlayer1")
@onready var hbox_player_1 = vbox_player_1.get_node("HBoxPlayer1")
@onready var label_player_1 = hbox_player_1.get_node("LabelPlayer1")
@onready var line_player_1 = hbox_player_1.get_node("LinePlayer1")
@onready var color_player_1 = hbox_player_1.get_node("ColorPlayer1")

@onready var panel_player_2 = vbox_players.get_node("PanelPlayer2")
@onready var vbox_player_2 = panel_player_2.get_node("MarginPlayer2/VBoxPlayer2")
@onready var check_player_2 = vbox_player_2.get_node("CheckPlayer2")
@onready var hbox_player_2 = vbox_player_2.get_node("HBoxPlayer2")
@onready var label_player_2 = hbox_player_2.get_node("LabelPlayer2")
@onready var line_player_2 = hbox_player_2.get_node("LinePlayer2")
@onready var color_player_2 = hbox_player_2.get_node("ColorPlayer2")

@onready var panel_player_3 = vbox_players.get_node("PanelPlayer3")
@onready var vbox_player_3 = panel_player_3.get_node("MarginPlayer3/VBoxPlayer3")
@onready var check_player_3 = vbox_player_3.get_node("CheckPlayer3")
@onready var hbox_player_3 = vbox_player_3.get_node("HBoxPlayer3")
@onready var label_player_3 = hbox_player_3.get_node("LabelPlayer3")
@onready var line_player_3 = hbox_player_3.get_node("LinePlayer3")
@onready var color_player_3 = hbox_player_3.get_node("ColorPlayer3")

@onready var panel_player_4 = vbox_players.get_node("PanelPlayer4")
@onready var vbox_player_4 = panel_player_4.get_node("MarginPlayer4/VBoxPlayer4")
@onready var check_player_4 = vbox_player_4.get_node("CheckPlayer4")
@onready var hbox_player_4 = vbox_player_4.get_node("HBoxPlayer4")
@onready var label_player_4 = hbox_player_4.get_node("LabelPlayer4")
@onready var line_player_4 = hbox_player_4.get_node("LinePlayer4")
@onready var color_player_4 = hbox_player_4.get_node("ColorPlayer4")

@onready var button_apply = vbox_players.get_node("ButtonApply")

@onready var margin_bottom = $VBoxSetup/MarginBottom
@onready var hbox_bottom = margin_bottom.get_node("HBoxBottom")
@onready var label_warning = hbox_bottom.get_node("LabelWarning")

@onready var popup_colors = $PopupColors
@onready var grid_colors = popup_colors.get_node("PanelColors/MarginColors/GridColors")
@onready var animation_setup = $AnimationSetup


func _on_ready():
    add_child(message_dialog)
    
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    margin_bottom.modulate = Color(1, 1, 1, 0)
    
    if OS.get_name() == "Web":
        build_file_dialog()
    set_vocabulary()
    set_players()
    animation_setup.play("fade_in")


func _on_tree_exiting():
    message_dialog.queue_free()


func _on_setup_local_animation_finished(animation_name):
    if animation_name == "fade_out":
        get_tree().change_scene_to_file(next_scene)


func _on_button_close_pressed():
    next_scene = "res://scenes/main.tscn"
    animation_setup.play("fade_out")


func _on_button_open_pressed():
    if OS.get_name() == "Web":
        JavaScriptBridge.eval("openFile()")
        return
    
    var current_dir = ""
    if FileAccess.file_exists(global.last_file):
        current_dir = global.last_file.get_base_dir()
    elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
        current_dir = global.default_dir
    var title = tr("Select a File")
    var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    var csv_filter = PackedStringArray(["*.csv ; CSV Files"])
    DisplayServer.file_dialog_show(title, current_dir, "", false, mode, csv_filter,
        _on_native_file_dialog_file_selected)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    
    var file = FileAccess.open(selected_paths[0], FileAccess.READ)
    get_vocabulary([selected_paths[0], file.get_as_text()])  


func _on_unit_button_toggled(_toggled):
    check_data_complete()


func _on_check_player_toggled(toggled, number):
    var nodes = []
    
    if number == 2 and toggled:
        nodes = [hbox_player_2, line_player_2, color_player_2, check_player_3]
        global.player_number = 2
    if number == 2 and not toggled:
        nodes = [hbox_player_2, hbox_player_3, hbox_player_4, line_player_2, line_player_3,
            line_player_4, color_player_2, color_player_3, color_player_4, check_player_3,
            check_player_4]
        global.player_number = 1
    if number == 3 and toggled:
        nodes = [hbox_player_3, line_player_3, color_player_3, check_player_4]
        global.player_number = 3
    if number == 3 and not toggled:
        nodes = [hbox_player_3, hbox_player_4, line_player_3, line_player_4, color_player_3,
            color_player_4, check_player_4]
        global.player_number = 2
    if number == 4 and toggled:
        nodes = [hbox_player_4, line_player_4, color_player_4]
        global.player_number = 4
    if number == 4 and not toggled:
        nodes = [hbox_player_4, line_player_4, color_player_4]
        global.player_number = 3
    
    enable_player_nodes(toggled, nodes)
    check_data_complete()


func _on_line_player_text_changed(_new_text):
    check_data_complete()


func _on_color_player_1_pressed():
    var pos = color_player_1.global_position
    var siz = popup_colors.size
    current_color_button = color_player_1
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_2_pressed():
    var pos = color_player_2.global_position
    var siz = popup_colors.size
    current_color_button = color_player_2
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_3_pressed():
    var pos = color_player_3.global_position
    var siz = popup_colors.size
    current_color_button = color_player_3
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_4_pressed():
    var pos = color_player_4.global_position
    var siz = popup_colors.size
    current_color_button = color_player_4
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_popup_color_pressed():   
    for color_button in grid_colors.get_children():
        color_button.text = "X" if color_button.button_pressed else ""


func _on_popup_colors_popup_hide():
    var button = grid_colors.get_child(0).button_group.get_pressed_button()
    current_color_button.add_theme_stylebox_override("normal", button.get_theme_stylebox("normal"))
    current_color_button.add_theme_stylebox_override("hover", button.get_theme_stylebox("hover"))
    current_color_button.add_theme_stylebox_override("pressed", button.get_theme_stylebox("pressed"))
    check_data_complete()


func _on_button_card_number_pressed(number):
    global.card_number = number


func _on_button_apply_pressed():
    var selected_units = []
    global.unit_vocables = []
    for child in flow_units.get_children():
        if child.button_pressed:
            selected_units.append(child.text)
    for vocable in global.vocables:
        if vocable.unit in selected_units:
            global.unit_vocables.append(vocable)
    
    if global.card_number > len(global.unit_vocables) * 2:
        var text = tr("The number of vocables is too small.\nPlease select more units or decrease the number of cards.")
        return show_message_dialog(text)
    
    global.player1.name = line_player_1.text
    global.player1.color = color_player_1.get_theme_stylebox("normal").bg_color
    if check_player_2.button_pressed:
        global.player2.name = line_player_2.text
        global.player2.color = color_player_2.get_theme_stylebox("normal").bg_color
    if check_player_3.button_pressed:
        global.player3.name = line_player_3.text
        global.player3.color = color_player_3.get_theme_stylebox("normal").bg_color
    if check_player_4.button_pressed:
        global.player4.name = line_player_4.text        
        global.player4.color = color_player_4.get_theme_stylebox("normal").bg_color
    
    next_scene = "res://scenes/game_local.tscn"
    animation_setup.play("fade_out")


func get_vocabulary(data):
    var path = data[0]
    var content = data[1]
    var csv_array = content.split("\n")
    global.unit_names = []
    global.vocables = []
    
    for i in range(csv_array.size()):
        var line_array = csv_array[i].replace(";",",").replace("\t",",").split(",")
        if line_array.size() == 3:
            global.vocables.append(global.vocable.new(line_array[0], line_array[1], line_array[2]))
            if not line_array[0] in global.unit_names:
                global.unit_names.append(line_array[0])

    if not global.vocables:
        var text = tr("Can't load vocabulary list!\nThe data structure is invalid.")
        for child in flow_units.get_children():
            flow_units.remove_child(child)
        global.last_file = ""
        label_file.text = tr("No File Loaded")
        return show_message_dialog(text)
    
    global.last_file = path
    label_file.text = path.get_file()
    add_unit_buttons()
    check_data_complete()


func add_unit_buttons():
    for child in flow_units.get_children():
        flow_units.remove_child(child)
    global.unit_names.sort_custom(func(a, b): return a.naturalnocasecmp_to(b) < 0)
    
    for unit_name in global.unit_names:
        var button = Button.new()
        button.add_theme_stylebox_override("hover", load("res://tres/unit_button_hovered.tres"))
        button.add_theme_stylebox_override("normal", load("res://tres/unit_button_normal.tres"))
        button.add_theme_stylebox_override("pressed", load("res://tres/unit_button_pressed.tres"))
        button.add_theme_stylebox_override("focus", StyleBoxEmpty.new())
        button.custom_minimum_size = Vector2(91, 0)
        button.text = str(unit_name)
        button.toggle_mode = true
        button.toggled.connect(_on_unit_button_toggled)
        flow_units.add_child(button)


func build_file_dialog():
    java_script_callback = JavaScriptBridge.create_callback(get_vocabulary)
    JavaScriptBridge.get_interface("web_file_dialog").dataLoaded = java_script_callback


func set_vocabulary():
    if FileAccess.file_exists(global.last_file):
        _on_native_file_dialog_file_selected(null, [global.last_file], null)
    elif FileAccess.file_exists(global.default_file):
        _on_native_file_dialog_file_selected(null, [global.default_file], null)
    if not global.card_number:
        global.card_number = global.default_card_number
    for button in hbox_number.get_children():
        if str(global.card_number) == button.get_child(0).text:
            button.button_pressed = true
            break


func set_players():
    line_player_1.text = global.player1.name
    color_player_1.get_theme_stylebox("normal").bg_color = global.player1.color
    if global.player_number > 1:
        check_player_2.set_pressed_no_signal(true)
        line_player_2.text = global.player2.name
        color_player_2.get_theme_stylebox("normal").bg_color = global.player2.color
        enable_player_nodes(true, [hbox_player_2, line_player_2, color_player_2, check_player_3])
    if global.player_number > 2:
        check_player_3.set_pressed_no_signal(true)
        line_player_3.text = global.player3.name
        color_player_3.get_theme_stylebox("normal").bg_color = global.player3.color
        enable_player_nodes(true, [hbox_player_3, line_player_3, color_player_3, check_player_4])
    if global.player_number > 3:
        check_player_4.set_pressed_no_signal(true)
        line_player_4.text = global.player4.name
        color_player_4.get_theme_stylebox("normal").bg_color = global.player4.color
        enable_player_nodes(true, [hbox_player_4, line_player_4, color_player_4])


func select_color_in_popup():
    var color = current_color_button.get_theme_stylebox("normal").bg_color
    for color_button in grid_colors.get_children():
        if color_button.get_theme_stylebox("normal").bg_color == color:
            color_button.text = "X"
            color_button.button_pressed = true
            color_button.grab_focus()
        else:
            color_button.text = ""


func enable_player_nodes(boolean, nodes):
    for node in nodes:
        if node is HBoxContainer:
            node.modulate = Color(1, 1, 1 ,1) if boolean else Color(1, 1, 1 ,0.5)
        elif node is LineEdit:
            node.editable = boolean
        else:
            node.disabled = not(boolean)
            if node.disabled and node is CheckBox:
                node.set_pressed_no_signal(false)


func check_data_complete():
    var error_text = ""
    
    error_text = check_vocabulary()
    if error_text:
        return enable_button_apply(false, error_text)
    
    error_text = check_player_names()
    if error_text:
        return enable_button_apply(false, error_text)
    
    error_text = check_player_colors()
    if error_text:
        return enable_button_apply(false, error_text)
    
    enable_button_apply(true, error_text)


func check_vocabulary():
    if flow_units.get_children() == []:
        return tr("Please load a vocabulary list!")
    
    var unit_selected = false
    for child in flow_units.get_children():
        if child.button_pressed:
            unit_selected = true
            break
    if not unit_selected:
        return tr("Please choose one unit at least!")
    
    return ""


func check_player_names():
    var error_text = tr("Please type in a name for each player!")
    
    if line_player_1.text == "":
        return error_text
    var player_names = [line_player_1.text]
    if global.player_number > 1:
        if line_player_2.text == "":
            return error_text
        player_names.append(line_player_2.text)
    if global.player_number > 2:
        if line_player_3.text == "":
            return error_text
        player_names.append(line_player_3.text)
    if global.player_number > 3:
        if line_player_4.text == "":
            return error_text
        player_names.append(line_player_4.text)
    for player_name in player_names:
        if player_names.count(player_name) > 1:
            return tr("Please type in different names for each player!")
    
    return ""


func check_player_colors():
    var player_colors = [color_player_1.get_theme_stylebox("normal").bg_color]
    if global.player_number > 1:
        player_colors.append(color_player_2.get_theme_stylebox("normal").bg_color)
    if global.player_number > 2:
        player_colors.append(color_player_3.get_theme_stylebox("normal").bg_color)
    if global.player_number > 3:
        player_colors.append(color_player_4.get_theme_stylebox("normal").bg_color)
    for player_color in player_colors:
        if player_colors.count(player_color) > 1:
            return tr("Please select different colors for each player!")
    
    return ""


func enable_button_apply(boolean, error_text):
    hbox_bottom.modulate = Color(1, 1, 1, 0) if boolean else Color(1, 1, 1, 1)
    label_warning.text = error_text
    button_apply.disabled = not(boolean)
    button_apply.get_child(0).modulate = Color(1, 1, 1, 1) if boolean else Color(1, 1, 1, 0.5)


func show_message_dialog(message_text):
    message_dialog.label_title.text = tr("Error!")
    message_dialog.label_main.text = message_text
    message_dialog.button_close.hide()
    message_dialog.popup_centered()
