class_name HighScore
extends Control


var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

@onready var margin_top = $VBoxHighScore/MarginTop
@onready var button_delete = margin_top.get_node("ButtonDelete")

@onready var margin_board = $VBoxHighScore/TextureBoard/MarginBoard
@onready var vbox_board = margin_board.get_node("VBoxBoard")
@onready var hbox_podium = vbox_board.get_node("HBoxPodium")
@onready var label_place1 = hbox_podium.get_node("TexturePodium/MarginPlace1/LabelPlace1")
@onready var label_place2 = hbox_podium.get_node("LabelPlace2")
@onready var label_place3 = hbox_podium.get_node("LabelPlace3")
@onready var tree_score = vbox_board.get_node("TreeScore")
@onready var label_empty = vbox_board.get_node("LabelEmpty")

@onready var margin_bottom = $VBoxHighScore/MarginBottom
@onready var animation_high_score = $AnimationHighScore


func _on_ready():
    add_child(message_dialog)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    animation_high_score.play("fade_in")
    
    setup_score_columns()
    global.load_high_score()
    setup_score_data()


func _on_tree_exiting():
    message_dialog.queue_free()


func _on_high_score_animation_finished(animation_name):
    if animation_name == "fade_out":
        var next_scene = "res://scenes/main.tscn"
        get_tree().change_scene_to_file(next_scene)


func _on_button_delete_pressed():
    show_message_dialog()


func _on_button_close_pressed():
    animation_high_score.play("fade_out")


func setup_score_columns():
    tree_score.set_column_title(0, tr("Pl."))
    tree_score.set_column_title(1, tr("Name"))
    tree_score.set_column_title(2, tr("Victories"))
    tree_score.set_column_title_alignment(0, 0)
    tree_score.set_column_title_alignment(1, 0)
    tree_score.set_column_title_alignment(2, 0)
    tree_score.set_column_expand_ratio(0, 1)
    tree_score.set_column_expand_ratio(1, 8)
    tree_score.set_column_expand_ratio(2, 1)


func setup_score_data():
    tree_score.clear()
    var root = tree_score.create_item()
    for i in range(len(global.high_score)):
        var item = tree_score.create_item(root)
        var place = i + 1
        item.set_text(0, "%02d" % place)
        item.set_text(1, global.high_score[i][0])
        item.set_text(2, str(global.high_score[i][1]))
            
    # modify place of players with equal number of victories
    for child in root.get_children():
        if child.get_next():
            if child.get_text(2) == child.get_next().get_text(2):
                child.get_next().set_text(0, "")
    
    if root.get_child_count() == 0:
        button_delete.hide()
        tree_score.hide()
        label_empty.show()
    else:
        button_delete.show()
        tree_score.show()
        label_empty.hide()
    
    if root.get_child_count() > 0:
        label_place1.text = "1. " + root.get_child(0).get_text(1)
    if root.get_child_count() > 1:
        label_place2.text = "2. " + root.get_child(1).get_text(1)
    if root.get_child_count() > 2:
        label_place3.text = "3. " + root.get_child(2).get_text(1)


func show_message_dialog():
    message_dialog.label_title.text = tr("Delete High Score?")
    message_dialog.label_main.text = tr("Do you want to delete the entire high score?")
    message_dialog.label_close.text = tr("Delete High Score")
    message_dialog.popup_centered()


func message_dialog_close_pressed():
    message_dialog.hide()
    
    global.high_score = []
    global.save_high_score()
    setup_score_data()
    label_place1.text = ""
    label_place2.text = ""
    label_place3.text = ""
