class_name GameOnline
extends Control


var card_pairs = []
var current_cards = []
var current_countdown = global.countdown_length
var current_player = global.player1
var game_stage = "game_start"
var moving_cards = []
var next_scene = ""
var pairs_counter = 0
var players = [global.player1]
var tries_counter = 0
var victory_dialog = null

var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()
var sound_finish = {"path": load("res://assets/sounds/finish.wav"), "db": 0}
var sound_found = {"path": load("res://assets/sounds/found.wav"), "db": 0}
var sound_start = {"path": load("res://assets/sounds/start.wav"), "db": 0}
var stylebox_label = load("res://tres/style_label_main.tres")

@onready var margin_top = $VBoxGame/MarginTop
@onready var label_current_player = margin_top.get_node("LabelCurrentPlayer")
@onready var label_countdown = margin_top.get_node("CenterTop/LabelCountdown")
@onready var button_menu = margin_top.get_node("ButtonMenu")

@onready var texture_board = $VBoxGame/TextureBoard
@onready var grid_board = texture_board.get_node("MarginBoard/GridBoard")

@onready var margin_bottom = $VBoxGame/MarginBottom
@onready var hbox_players = margin_bottom.get_node("VBoxBottom/HBoxPlayers")
@onready var color_player1 = hbox_players.get_node("ColorPlayer1")
@onready var label_player1 = color_player1.get_node("MarginPlayer1/HBoxPlayer1/LabelPlayer1")
@onready var label_score1 = color_player1.get_node("MarginPlayer1/HBoxPlayer1/LabelScore1")
@onready var color_player2 = hbox_players.get_node("ColorPlayer2")
@onready var label_player2 = color_player2.get_node("MarginPlayer2/HBoxPlayer2/LabelPlayer2")
@onready var label_score2 = color_player2.get_node("MarginPlayer2/HBoxPlayer2/LabelScore2")
@onready var color_player3 = hbox_players.get_node("ColorPlayer3")
@onready var label_player3 = color_player3.get_node("MarginPlayer3/HBoxPlayer3/LabelPlayer3")
@onready var label_score3 = color_player3.get_node("MarginPlayer3/HBoxPlayer3/LabelScore3")
@onready var color_player4 = hbox_players.get_node("ColorPlayer4")
@onready var label_player4 = color_player4.get_node("MarginPlayer4/HBoxPlayer4/LabelPlayer4")
@onready var label_score4 = color_player4.get_node("MarginPlayer4/HBoxPlayer4/LabelScore4")
@onready var hbox_statistics = margin_bottom.get_node("VBoxBottom/HBoxStatistics")
@onready var label_pairs = hbox_statistics.get_node("HBoxPairs/LabelPairs")
@onready var label_tries = hbox_statistics.get_node("HBoxTries/LabelTries")

@onready var animation_game = $AnimationGame
@onready var audio_player = $AudioPlayer
@onready var popup_settings = $PopupSettings
@onready var timer_countdown = $TimerCountdown


func _on_ready():
    add_nodes()
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_bottom.modulate = Color(1, 1, 1, 0)
    
    create_button_cards()
    deal_cards()


func _process(_delta):
    global.peer.poll()
    var state = global.peer.get_ready_state()
    if state == WebSocketPeer.STATE_OPEN:
        while global.peer.get_available_packet_count():
            packet_received(global.peer.get_packet())


func _on_tree_exiting():
    message_dialog.queue_free()


func _on_game_animation_finished(animation_name):
    if animation_name == "move":
        animation_game.get_animation("move").clear()
        for moving_card in moving_cards:
            self.remove_child(moving_card)
        animation_game.play("fade_in")
        if global.host:
            get_vocables()
        set_players()
    
    if animation_name == "fade_in":
        current_countdown = get_timer_length()
        label_countdown.text = tr("Game Start: %s s") % str(current_countdown)
        timer_countdown.start()
    
    if animation_name == "fade_out":
        if next_scene == "res://scenes/main.tscn":
            global.peer.close()
            get_tree().change_scene_to_file(next_scene)
        elif next_scene == "res://scenes/setup_online.tscn":
            if global.host:
                send_packet(global.clients, "game_started", "false")
            get_tree().change_scene_to_file(next_scene)
        else:
            global.peer.close()
            get_tree().quit()


func _on_menu_item_pressed(id):
    if id == 0: # New Game
        next_scene = "res://scenes/setup_online.tscn"
        animation_game.play("fade_out")
    if id == 1: # Settings
        global.last_scene = "game_online"
        var scene = load("res://scenes/settings.tscn").instantiate()
        popup_settings.add_child(scene)
        popup_settings.popup()
    if id == 2: # Main Menu
        if pairs_counter * 2 == global.card_number:
            next_scene = "res://scenes/main.tscn"
            animation_game.play("fade_out")
        else:
            show_message_dialog("main_menu")
    if id == 3: # Quit Game
        if pairs_counter * 2 == global.card_number:
            next_scene = ""
            animation_game.play("fade_out")
        else:
            show_message_dialog("quit")


func _on_button_card_pressed(button):
    button.disabled = true
    button.pivot_offset.x = button.size.x / 2
    button.animation.play("flip")
    timer_countdown.paused = true

    if current_player.number == global.client_index + 1:
        send_packet(global.clients, "button_pressed", str(button.get_index()))
    

func _on_button_card_animation_finished(_animation_name, button):
    button.label.show()
    current_cards.append(button)
    timer_countdown.paused = false
    if current_cards.size() > 1:
        tries_counter += 1
        label_tries.text = str(tries_counter)
        check_pair_found()


func _on_timer_countdown_timeout():
    current_countdown -= 1
    if current_countdown > 0:
        if game_stage == "game_start":
            label_countdown.text = tr("Game Start: %s s") % str(current_countdown)
        elif game_stage == "find_cards":
            label_countdown.text = tr("Time Left: %s s") % str(current_countdown)
        elif game_stage == "next_player":
            label_countdown.text = tr("Next Player: %s s") % str(current_countdown)
    else:
        if game_stage == "game_start" or game_stage == "next_player":
            stage_find_cards()
        elif game_stage == "find_cards":
            stage_next_player()
    timer_countdown.start()


func add_nodes():
    add_child(message_dialog)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    
    var popup = button_menu.get_popup()
    popup.add_theme_constant_override("item_start_padding", 8)
    popup.add_theme_constant_override("item_end_padding", 8)
    popup.add_theme_constant_override("h_separation", 8)
    popup.add_theme_constant_override("v_separation", 8)
    popup.add_theme_color_override("font_color", Color(1, 1, 1, 1))
    popup.add_theme_color_override("font_separator_color", Color(1, 1, 1, 1))
    popup.id_pressed.connect(_on_menu_item_pressed)


func create_button_cards():
    for n in range(0, global.card_number):
        var button = load("res://scenes/button_card.tscn").instantiate()
        button.pressed.connect(_on_button_card_pressed.bind(button))
        button.get_node("AnimationCard").animation_finished.connect(
            _on_button_card_animation_finished.bind(button))
        button.disabled = true
        grid_board.add_child(button)


func deal_cards():
    await get_tree().create_timer(0.1).timeout
    var index = 0
    var start_time = 0
    
    for card in grid_board.get_children():
        var moving_card = card.duplicate()
        self.add_child(moving_card)
        moving_card.modulate = Color(1, 1, 1, 1)
        moving_card.position = Vector2(960 - card.size.x / 2.0, 1080)
        moving_card.set_deferred("size", card.size)
        moving_cards.append(moving_card)
        
        var animation = animation_game.get_animation("move")
        animation.length = (len(grid_board.get_children()) + 1) / 10.0
        add_animation(animation, index, NodePath(str(moving_card.get_path()) + ":position"), 0,
            start_time, animation.length, moving_card.position, card.position + Vector2(16, 80))
        index += 1
        add_animation(animation, index, NodePath(str(moving_card.get_path()) + ":visible"), 1,
            start_time, animation.length, true, false)
        index += 1
        add_animation(animation, index, NodePath(str(card.get_path()) + ":modulate"), 1,
            start_time, animation.length, Color(1, 1, 1, 0), Color(1, 1, 1, 1))
        index += 1
        start_time += 0.1
    
    animation_game.play("move")
    play_sound(sound_start)


func set_players():
    global.player1.label_score = label_score1
    global.player2.label_score = label_score2
    global.player3.label_score = label_score3
    global.player4.label_score = label_score4
    global.player1.score = 0
    global.player2.score = 0
    global.player3.score = 0
    global.player4.score = 0
    
    label_player1.text = global.player1.name
    recolor_node(color_player1, [label_player1, label_score1], global.player1.color)
    hbox_players.show()
    
    if 1 in global.clients:
        label_player2.text = global.player2.name
        recolor_node(color_player2, [label_player2, label_score2], global.player2.color)
        players.append(global.player2)
        color_player2.show()

    if 2 in global.clients:
        label_player3.text = global.player3.name
        recolor_node(color_player3, [label_player3, label_score3], global.player3.color)
        players.append(global.player3)
        color_player3.show()
    
    if 3 in global.clients:
        label_player4.text = global.player4.name
        recolor_node(color_player4, [label_player4, label_score4], global.player4.color)
        players.append(global.player4)
        color_player4.show()

    if global.host:
        current_player = players.pick_random()
        show_current_player()
        recolor_node(stylebox_label, [label_current_player], current_player.color)
        send_packet(global.clients, "current_player", str(current_player.number))


func get_vocables():
    var sorted_vocables = []
    var unsorted_vocables = []
    
    global.unit_vocables.shuffle()
    for i in range(0, global.card_number / 2.0):
        card_pairs.append([global.unit_vocables[i].source, global.unit_vocables[i].target])
    for pair in card_pairs:
        unsorted_vocables.append_array(pair)
    send_packet(global.clients, "card_pairs", "\t".join(unsorted_vocables))
    
    for button in grid_board.get_children():
        button.label.text = unsorted_vocables.pick_random()
        sorted_vocables.append(button.label.text)
        unsorted_vocables.erase(button.label.text)
    send_packet(global.clients, "button_vocables", "\t".join(sorted_vocables))
    
    label_pairs.text = str(0) + " / " + str(global.card_number / 2.0)


func add_animation(animation, index, path, mode, time1, time2, value1, value2):
    var track = animation.add_track(Animation.TYPE_VALUE)
    animation.value_track_set_update_mode(track, mode)
    animation.track_set_path(track, path)
    animation.track_insert_key(index, time1, value1)
    animation.track_insert_key(index, time2, value2)


func stage_next_player():
    for card in grid_board.get_children():
        if not card.found:
            card.disabled = true
    game_stage = "next_player"
    current_cards = []
    current_countdown = get_timer_length()
    label_countdown.text = tr("Next Player: %s s") % str(current_countdown)
    label_current_player.hide()
    if global.host:
        get_next_player()


func stage_find_cards():
    for card in grid_board.get_children():
        if not card.found:
            card.label.hide()
    game_stage = "find_cards"
    current_countdown = get_timer_length()
    label_countdown.text = tr("Time Left: %s s") % str(current_countdown)
    label_current_player.show()
    enable_disable_cards(current_player.number)


func get_timer_length():
    if game_stage == "game_start" or game_stage == "next_player":
        if global.timer_mode == 0:
            return 3
        if global.timer_mode == 1:
            return 5
        if global.timer_mode == 2:
            return 7
    if game_stage == "find_cards":
        if global.timer_mode == 0:
            return 10
        if global.timer_mode == 1:
            return 15
        if global.timer_mode == 2:
            return 20


func check_pair_found():
    var current_pair = [current_cards[0].label.text, current_cards[1].label.text]
    current_pair.sort()
    for pair in card_pairs:
        pair.sort()
        if pair == current_pair:
            return pair_found()
    no_pair_found()


func pair_found():
    current_player.score += 1
    current_player.label_score.text = str(current_player.score)
    pairs_counter += 1
    label_pairs.text = str(pairs_counter) + " / " + str(global.card_number / 2.0)
    for card in current_cards:
        card.found = true
        recolor_node(card, [card.label], current_player.color)
    current_cards = []
    
    if pairs_counter * 2 == global.card_number:
        game_finished()
        play_sound(sound_finish)
    else:
        current_countdown = get_timer_length()
        label_countdown.text = tr("Time Left: %s s") % current_countdown
        timer_countdown.start()
        play_sound(sound_found)


func no_pair_found():
    stage_next_player()
    timer_countdown.start()


func get_next_player():
    var index = players.find(current_player)
    if index < players.size() - 1:
        current_player = players[index + 1]
    else: 
        current_player = players[0]
    show_current_player()
    recolor_node(stylebox_label, [label_current_player], current_player.color)
    
    send_packet(global.clients, "current_player", str(current_player.number))


func enable_disable_cards(player_number):
    if game_stage == "game_start":
        return
    
    for card in grid_board.get_children():
        if player_number == global.client_index + 1:
            if not card.found:
                card.disabled = false
        else:
            card.disabled = true


func show_current_player():
    if current_player.number == global.client_index + 1:
        label_current_player.text = tr("Player: You (%s)") % current_player.name
    else:
        label_current_player.text = tr("Player: %s") % current_player.name


func recolor_node(bg_node, fg_nodes, color):
    if bg_node.get_class() == "ColorRect":
        bg_node.color = color
    elif bg_node.get_class() == "StyleBoxFlat":
        bg_node.bg_color = color
    else:
        bg_node.self_modulate = color
    
    for fg_node in fg_nodes:
        if color.get_luminance() < 0.5:
            fg_node.add_theme_color_override("font_color", Color(1, 1, 1, 1))
        else:
            fg_node.add_theme_color_override("font_color", Color(0, 0, 0, 1))


func game_finished():
    game_stage = "game_finished"
    button_menu.get_popup().set_item_disabled(0, false)
    label_countdown.hide()
    label_current_player.hide()
    timer_countdown.stop()
    
    if global.player1.score > [global.player2.score, global.player3.score, global.player4.score].max():
        global.winner = global.player1
    elif global.player2.score > [global.player1.score, global.player3.score, global.player4.score].max():
        global.winner = global.player2
    elif global.player3.score > [global.player1.score, global.player2.score, global.player4.score].max():
        global.winner = global.player3
    elif global.player4.score > [global.player1.score, global.player2.score, global.player3.score].max():
        global.winner = global.player4
    else:
        global.winner = null

    victory_dialog = load("res://scenes/victory_dialog.tscn").instantiate()
    add_child(victory_dialog)
    victory_dialog.button_main.pressed.connect(dialog_button_main_pressed)
    victory_dialog.button_score.pressed.connect(dialog_button_score_pressed)
    victory_dialog.button_score.get_child(0).text = tr("Rematch")
    victory_dialog.popup_centered()


func play_sound(sound_data):
    if not global.sounds:
        return
    
    if sound_data["path"].resource_path.contains("start.wav"):
        if global.card_number == 16:
            audio_player.pitch_scale = 0.85
        elif global.card_number == 20:
            audio_player.pitch_scale = 0.70
        elif global.card_number == 24:
            audio_player.pitch_scale = 0.55
    audio_player.stream = sound_data["path"]
    audio_player.volume_db = sound_data["db"]
    audio_player.play()


func send_packet(receivers, topic, data):
    var packet = {"receivers": receivers, "topic": topic, "data": data}
    global.peer.send_text(JSON.stringify(packet, "  "))


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]

    if topic == "client_disconnected":
        global.clients.erase(int(data[0]))
        if game_stage == "game_finished":
            return
        if int(data[0]) == 0:
            label_current_player.hide()
            label_countdown.hide()
            timer_countdown.stop()
            show_message_dialog("host_left")
        else:
            if int(data[0]) == 1:
                global.player2.score = 0
                color_player2.modulate = Color(1, 1, 1, 0.25)
                players.erase(global.player2)
            elif int(data[0]) == 2:
                global.player3.score = 0
                color_player3.modulate = Color(1, 1, 1, 0.25)
                players.erase(global.player3)
            elif int(data[0]) == 4:
                global.player4.score = 0
                color_player4.modulate = Color(1, 1, 1, 0.25)
                players.erase(global.player4)
            if global.clients.size() == 1:
                label_current_player.hide()
                label_countdown.hide()
                timer_countdown.stop()
                show_message_dialog("last_opponent_left")
        return
    
    if topic == "current_player":
        if data == "1":
            current_player = global.player1
        if data == "2":
            current_player = global.player2
        if data == "3":
            current_player = global.player3
        if data == "4":
            current_player = global.player4
        enable_disable_cards(int(data))
        show_current_player()
        recolor_node(stylebox_label, [label_current_player], current_player.color)
        return

    if topic == "card_pairs":
        var vocables = []
        for vocable in data.split("\t"):
            vocables.append(vocable)
        for i in range(0, len(vocables), 2):
            card_pairs.append([vocables[i], vocables[i+1]])
        label_pairs.text = str(0) + " / " + str(global.card_number / 2.0)
        return

    if topic == "button_vocables":
        var index = 0
        var vocables = []
        for vocable in data.split("\t"):
            vocables.append(vocable)
        for button in grid_board.get_children():
            button.label.text = vocables[index]
            index += 1
        return

    if topic == "button_pressed":
        var index = int(data)
        var button = grid_board.get_child(index)
        _on_button_card_pressed(button)


func dialog_button_main_pressed():
    victory_dialog.hide()
    next_scene = "res://scenes/main.tscn"
    animation_game.play("fade_out")


func dialog_button_score_pressed():
    victory_dialog.hide()
    next_scene = "res://scenes/setup_online.tscn"
    animation_game.play("fade_out")


func show_message_dialog(case):
    if case == "host_left" or case == "last_opponent_left":
        message_dialog.label_title.text = tr("Game Over!")
        message_dialog.label_close.text = tr("Main Menu")
        message_dialog.button_back.hide()
        if case == "host_left":
            message_dialog.label_main.text = tr("The game was canceled by the host.")
        elif case == "last_opponent_left":
            message_dialog.label_main.text = tr("The last opponent left the game.")
    else:
        message_dialog.button_back.show()
        if case == "new_game":
            message_dialog.label_title.text = tr("Abort Game?")
            message_dialog.label_main.text = tr("Do you want to start a new game?")
            message_dialog.label_close.text = tr("New Game")
        elif case == "main_menu":
            message_dialog.label_title.text = tr("Abort Game?")
            message_dialog.label_main.text = tr("Do you want to return to the main menu?")
            message_dialog.label_close.text = tr("Main Menu") 
        elif case == "quit":
            message_dialog.label_title.text = tr("Quit Game?")
            message_dialog.label_main.text = tr("Do you want to quit the application?")
            message_dialog.label_close.text = tr("Quit Game")
    message_dialog.popup_centered()


func message_dialog_close_pressed():
    message_dialog.hide()
    if message_dialog.label_close.text == tr("Main Menu"):
        next_scene = "res://scenes/main.tscn"
        animation_game.play("fade_out")
        global.peer.close()
    elif message_dialog.label_close.text == tr("Quit Game"):
        next_scene = ""
        animation_game.play("fade_out")
        global.peer.close()
