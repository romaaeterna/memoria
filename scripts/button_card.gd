class_name Card
extends Button


var animation
var color = Color(1, 1, 1, 1)
var found = false
var label


func _on_ready():
    self.animation = $AnimationCard
    self.label = $MarginCard/LabelCard


func _on_tree_exiting():
    self.queue_free()
