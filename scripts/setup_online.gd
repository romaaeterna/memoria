class_name SetupOnline
extends Control


var current_color_button = null
var java_script_callback = null
var next_scene = ""

var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()
var message_no_name = tr("Please type in your name!")
var message_no_players = tr("Waiting for other players …")
var message_ready = tr("Click \"Ready\" when your are finished!")
var message_ready_all = tr("Waiting for all players to be ready …")
var message_same_color = tr("Please select a different color!")
var message_same_name = tr("Please type in a different name!")
var message_start_server = tr("Click \"Start Game\"!")
var message_start_client = tr("Waiting for game start by host …")

@onready var margin_top = $VBoxSetup/MarginTop
@onready var button_apply = margin_top.get_node("ButtonApply")

@onready var margin_board = $VBoxSetup/TextureBoard/MarginBoard
@onready var hbox_board = margin_board.get_node("HBoxBoard")
@onready var vbox_file = hbox_board.get_node("PanelFile/MarginFile/VBoxFile")
@onready var hbox_file = vbox_file.get_node("HBoxFile")
@onready var button_open = hbox_file.get_node("ButtonOpen")
@onready var label_file = hbox_file.get_node("LabelFile")
@onready var flow_units = vbox_file.get_node("PanelUnits/MarginUnits/ScrollUnits/FlowUnits")
@onready var vbox_cards = vbox_file.get_node("VBoxCards")
@onready var hbox_cards = vbox_cards.get_node("HBoxCards")
@onready var vbox_timer = vbox_file.get_node("VBoxTimer")
@onready var hbox_timer = vbox_timer.get_node("HBoxTimer")

@onready var vbox_players = hbox_board.get_node("PanelPlayers/MarginPlayers/VBoxPlayers")
@onready var panel_player_1 = vbox_players.get_node("PanelPlayer1")
@onready var vbox_player_1 = panel_player_1.get_node("MarginPlayer1/VBoxPlayer1")
@onready var check_player_1 = vbox_player_1.get_node("HBoxReady1/CheckPlayer1")
@onready var ready_player_1 = vbox_player_1.get_node("HBoxReady1/ReadyPlayer1")
@onready var hbox_player_1 = vbox_player_1.get_node("HBoxPlayer1")
@onready var label_player_1 = hbox_player_1.get_node("LabelPlayer1")
@onready var line_player_1 = hbox_player_1.get_node("LinePlayer1")
@onready var color_player_1 = hbox_player_1.get_node("ColorPlayer1")

@onready var panel_player_2 = vbox_players.get_node("PanelPlayer2")
@onready var vbox_player_2 = panel_player_2.get_node("MarginPlayer2/VBoxPlayer2")
@onready var check_player_2 = vbox_player_2.get_node("HBoxReady2/CheckPlayer2")
@onready var ready_player_2 = vbox_player_2.get_node("HBoxReady2/ReadyPlayer2")
@onready var hbox_player_2 = vbox_player_2.get_node("HBoxPlayer2")
@onready var label_player_2 = hbox_player_2.get_node("LabelPlayer2")
@onready var line_player_2 = hbox_player_2.get_node("LinePlayer2")
@onready var color_player_2 = hbox_player_2.get_node("ColorPlayer2")

@onready var panel_player_3 = vbox_players.get_node("PanelPlayer3")
@onready var vbox_player_3 = panel_player_3.get_node("MarginPlayer3/VBoxPlayer3")
@onready var check_player_3 = vbox_player_3.get_node("HBoxReady3/CheckPlayer3")
@onready var ready_player_3 = vbox_player_3.get_node("HBoxReady3/ReadyPlayer3")
@onready var hbox_player_3 = vbox_player_3.get_node("HBoxPlayer3")
@onready var label_player_3 = hbox_player_3.get_node("LabelPlayer3")
@onready var line_player_3 = hbox_player_3.get_node("LinePlayer3")
@onready var color_player_3 = hbox_player_3.get_node("ColorPlayer3")

@onready var panel_player_4 = vbox_players.get_node("PanelPlayer4")
@onready var vbox_player_4 = panel_player_4.get_node("MarginPlayer4/VBoxPlayer4")
@onready var check_player_4 = vbox_player_4.get_node("HBoxReady4/CheckPlayer4")
@onready var ready_player_4 = vbox_player_4.get_node("HBoxReady4/ReadyPlayer4")
@onready var hbox_player_4 = vbox_player_4.get_node("HBoxPlayer4")
@onready var label_player_4 = hbox_player_4.get_node("LabelPlayer4")
@onready var line_player_4 = hbox_player_4.get_node("LinePlayer4")
@onready var color_player_4 = hbox_player_4.get_node("ColorPlayer4")

@onready var margin_bottom = $VBoxSetup/MarginBottom
@onready var hbox_bottom = margin_bottom.get_node("HBoxBottom")
@onready var icon_warning = hbox_bottom.get_node("IconWarning")
@onready var label_network = hbox_bottom.get_node("LabelNetwork")

@onready var popup_colors = $PopupColors
@onready var grid_colors = popup_colors.get_node("PanelColors/MarginColors/GridColors")
@onready var animation_setup = $AnimationSetup
@onready var file_dialog = null


func _on_ready():
    add_child(message_dialog)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    margin_bottom.modulate = Color(1, 1, 1, 0)
    
    if OS.get_name() == "Web":
        build_file_dialog()
    setup_multiplayer()
    setup_right_panel()
    animation_setup.play("fade_in")


func _process(_delta):
    global.peer.poll()
    var state = global.peer.get_ready_state()
    if state == WebSocketPeer.STATE_OPEN:
        while global.peer.get_available_packet_count():
            packet_received(global.peer.get_packet())


func _on_tree_exiting():
    message_dialog.queue_free()


func _on_setup_online_animation_finished(animation_name):
    if animation_name == "fade_out":
        get_tree().change_scene_to_file(next_scene)


func _on_button_close_pressed():
    global.peer.close()
    next_scene = "res://scenes/main.tscn"
    animation_setup.play("fade_out")


func _on_button_open_pressed():
    if OS.get_name() == "Web":
        JavaScriptBridge.eval("openFile()")
        return
    
    var current_dir = ""
    if FileAccess.file_exists(global.last_file):
        current_dir = global.last_file.get_base_dir()
    elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
        current_dir = global.default_dir
    var title = tr("Select a File")
    var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    var csv_filter = PackedStringArray(["*.csv ; CSV Files"])
    DisplayServer.file_dialog_show(title, current_dir, "", false, mode, csv_filter,
        _on_native_file_dialog_file_selected)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    var file = FileAccess.open(selected_paths[0], FileAccess.READ)
    get_vocabulary([selected_paths[0], file.get_as_text()])


func _on_unit_button_toggled(toggled):
    check_player_ready(ready_player_1, global.player1.name, global.player1.color)
    for button in flow_units.get_children():
        if button.has_focus():
            var boolean = "true" if toggled else "false"
            var unit_string = str(button.get_index()) + "\t" + boolean
            return send_packet(global.clients, "unit_selected", unit_string)


func _on_button_card_number_pressed(number):
    global.card_number = number
    send_packet(global.clients, "button_card_number", str(number))


func _on_button_timer_pressed(index):
    global.timer_mode = index
    send_packet(global.clients, "timer_mode", str(index))


func _on_line_player_1_text_changed(new_name):
    global.player1.name = new_name
    check_player_ready(ready_player_1, global.player1.name, global.player1.color)
    send_packet(global.clients, "line_player_1", new_name)


func _on_line_player_2_text_changed(new_name):
    global.player2.name = new_name
    check_player_ready(ready_player_2, global.player2.name, global.player2.color)
    send_packet(global.clients, "line_player_2", new_name)


func _on_line_player_3_text_changed(new_name):
    global.player3.name = new_name
    check_player_ready(ready_player_3, global.player3.name, global.player3.color)
    send_packet(global.clients, "line_player_3", new_name)


func _on_line_player_4_text_changed(new_name):
    global.player4.name = new_name
    check_player_ready(ready_player_4, global.player4.name, global.player4.color)
    send_packet(global.clients, "line_player_4", new_name)


func _on_color_player_1_pressed():
    var pos = color_player_1.global_position
    var siz = popup_colors.size
    current_color_button = color_player_1
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_2_pressed():
    var pos = color_player_2.global_position
    var siz = popup_colors.size
    current_color_button = color_player_2
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_3_pressed():
    var pos = color_player_3.global_position
    var siz = popup_colors.size
    current_color_button = color_player_3
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_color_player_4_pressed():
    var pos = color_player_4.global_position
    var siz = popup_colors.size
    current_color_button = color_player_4
    popup_colors.popup(Rect2i(pos.x - siz.x + 54, pos.y - 4, siz.x, siz.y))
    select_color_in_popup()


func _on_popup_color_pressed():   
    for color_button in grid_colors.get_children():
        color_button.text = "X" if color_button.button_pressed else ""


func _on_popup_colors_popup_hide():
    var button = grid_colors.get_child(0).button_group.get_pressed_button()
    current_color_button.add_theme_stylebox_override("normal", button.get_theme_stylebox("normal"))
    current_color_button.add_theme_stylebox_override("hover", button.get_theme_stylebox("hover"))
    current_color_button.add_theme_stylebox_override("pressed", button.get_theme_stylebox("pressed"))
    
    var button_name = current_color_button.name.to_snake_case()
    var button_color = button.get_theme_stylebox("normal").bg_color
    if button_name.ends_with("1"):
        global.player1.color = button_color
        check_player_ready(ready_player_1, global.player1.name, global.player1.color)
    elif button_name.ends_with("2"):
        global.player2.color = button_color
        check_player_ready(ready_player_2, global.player2.name, global.player2.color)
    elif button_name.ends_with("3"):
        global.player3.color = button_color
        check_player_ready(ready_player_3, global.player3.name, global.player3.color)
    elif button_name.ends_with("4"):
        global.player4.color = button_color
        check_player_ready(ready_player_4, global.player4.name, global.player4.color)
    send_packet(global.clients, button_name, button_color.to_html())


func _on_ready_player_1_toggled(toggled):
    if not ready_player_1.disabled and not toggled:
        change_label_network(message_ready, false)
    elif toggled:
        var error_text = check_vocabulary()
        if error_text:
            enable_button_apply(error_text, true)
        else:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
    
    var boolean = "true" if toggled else "false"
    send_packet(global.clients, "ready_player_1", boolean)


func _on_ready_player_2_toggled(toggled):
    if not ready_player_2.disabled and not toggled:
        change_label_network(message_ready, false)
    elif toggled:
        var message = check_all_players_ready()
        enable_button_apply(message, false)
    
    var boolean = "true" if toggled else "false"
    send_packet(global.clients, "ready_player_2", boolean)


func _on_ready_player_3_toggled(toggled):
    if not ready_player_3.disabled and not toggled:
        change_label_network(message_ready, false)
    elif toggled:
        var message = check_all_players_ready()
        enable_button_apply(message, false)
    
    var boolean = "true" if toggled else "false"
    send_packet(global.clients, "ready_player_3", boolean)


func _on_ready_player_4_toggled(toggled):
    if not ready_player_4.disabled and not toggled:
        change_label_network(message_ready, false)
    elif toggled:
        var message = check_all_players_ready()
        enable_button_apply(message, false)
    
    var boolean = "true" if toggled else "false"
    send_packet(global.clients, "ready_player_4", boolean)


func _on_button_apply_pressed():
    var selected_units = []
    global.unit_vocables = []
    for child in flow_units.get_children():
        if child.button_pressed:
            selected_units.append(child.text)
    for vocable in global.vocables:
        if vocable.unit in selected_units:
            global.unit_vocables.append(vocable)
    
    if global.card_number > len(global.unit_vocables) * 2:
        var text = tr("The number of vocables is too small.\nPlease select more units or decrease the number of cards.")
        return show_message_dialog(text)
    
    send_packet(global.clients, "game_started", "true")
    next_scene = "res://scenes/game_online.tscn"
    animation_setup.play("fade_out")


func build_file_dialog():
    java_script_callback = JavaScriptBridge.create_callback(get_vocabulary)
    JavaScriptBridge.get_interface("web_file_dialog").dataLoaded = java_script_callback


func setup_multiplayer():
    if global.host:
        setup_left_panel()
    else:
        if 0 in global.clients:
            send_packet([0], "get_host_data", [global.client_index])
        else:
            var text = tr("The lobby was closed by the host.")
            show_message_dialog(text)


func get_vocabulary(data):
    var path = data[0]
    var content = data[1]
    var csv_array = content.split("\n")
    global.unit_names = []
    global.vocables = []
    
    for i in range(csv_array.size()):
        var line_array = csv_array[i].replace(";",",").replace("\t",",").split(",")
        if line_array.size() == 3:
            global.vocables.append(global.vocable.new(line_array[0], line_array[1], line_array[2]))
            if not line_array[0] in global.unit_names:
                global.unit_names.append(line_array[0])
    
    if not global.vocables:
        var text = tr("Can't load vocabulary list!\nThe data structure is invalid.")
        for child in flow_units.get_children():
            flow_units.remove_child(child)
        global.last_file = ""
        label_file.text = tr("No File Loaded")
        return show_message_dialog(text)
    
    global.last_file = path
    label_file.text = path.get_file()
    add_unit_buttons()
    check_player_ready(ready_player_1, global.player1.name, global.player1.color)
    
    send_packet(global.clients, "label_file", label_file.text)
    send_packet(global.clients, "unit_names", "\t".join(global.unit_names))


func add_unit_buttons():
    for child in flow_units.get_children():
        flow_units.remove_child(child)
    global.unit_names.sort_custom(func(a, b): return a.naturalnocasecmp_to(b) < 0)
    
    for unit_name in global.unit_names:
        var button = Button.new()
        button.add_theme_stylebox_override("hover", load("res://tres/unit_button_hovered.tres"))
        button.add_theme_stylebox_override("normal", load("res://tres/unit_button_normal.tres"))
        button.add_theme_stylebox_override("pressed", load("res://tres/unit_button_pressed.tres"))
        button.add_theme_stylebox_override("focus", StyleBoxEmpty.new())
        button.custom_minimum_size = Vector2(91, 0)
        button.text = str(unit_name)
        button.toggle_mode = true
        if not global.host:
            flow_units.modulate = Color(1, 1, 1, 0.5)
            button.mouse_filter = MOUSE_FILTER_IGNORE
        else:
            button.toggled.connect(_on_unit_button_toggled)
        flow_units.add_child(button)


func setup_left_panel():
    var color = Color(1, 1, 1, 1)
    button_apply.show()
    hbox_file.modulate = color
    button_open.disabled = false
    vbox_cards.modulate = color
    vbox_timer.modulate = color
    for button in hbox_cards.get_children():
        button.disabled = false
    for button in hbox_timer.get_children():
        button.disabled = false
    
    if FileAccess.file_exists(global.last_file):
        _on_native_file_dialog_file_selected(null, [global.last_file], null)
    elif FileAccess.file_exists(global.default_file):
        _on_native_file_dialog_file_selected(null, [global.default_file], null)
    if not global.card_number:
        global.card_number = global.default_card_number
    for button in hbox_cards.get_children():
        if str(global.card_number) == button.get_child(0).text:
            button.button_pressed = true
            break
    for button in hbox_timer.get_children():
        if global.timer_mode == button.get_index():
            button.button_pressed = true
            break


func setup_right_panel():
    if global.client_index == 0:
        line_player_1.text = global.player1.name
        select_color_for_button(color_player_1, global.player1.color.to_html())
        enable_player_nodes(true, [check_player_1, label_player_1, line_player_1, color_player_1])
        check_player_ready(ready_player_1, global.player1.name, global.player1.color)
        if len(global.clients) > 1:
            host_returned()
    if global.client_index == 1:
        line_player_2.text = global.player2.name
        select_color_for_button(color_player_2, global.player2.color.to_html())
        enable_player_nodes(true, [check_player_2, label_player_2, line_player_2, color_player_2])
        check_player_ready(ready_player_2, global.player2.name, global.player2.color)
        send_packet(global.clients, "check_player_2", "true")
        send_packet(global.clients, "color_player_2", global.player2.color.to_html())
    if global.client_index == 2:
        line_player_3.text = global.player3.name
        select_color_for_button(color_player_3, global.player3.color.to_html())
        enable_player_nodes(true, [check_player_3, label_player_3, line_player_3, color_player_3])
        check_player_ready(ready_player_3, global.player3.name, global.player3.color)
        send_packet(global.clients, "check_player_3", "true")
        send_packet(global.clients, "color_player_3", global.player3.color.to_html())
    if global.client_index == 3:
        line_player_4.text = global.player4.name
        select_color_for_button(color_player_4, global.player4.color.to_html())
        enable_player_nodes(true, [check_player_4, label_player_4, line_player_4, color_player_4])
        check_player_ready(ready_player_4, global.player4.name, global.player4.color)
        send_packet(global.clients, "check_player_4", "true")
        send_packet(global.clients, "color_player_4", global.player4.color.to_html())


func select_color_in_popup():
    var color = current_color_button.get_theme_stylebox("normal").bg_color
    for color_button in grid_colors.get_children():
        if color_button.get_theme_stylebox("normal").bg_color == color:
            color_button.text = "X"
            color_button.button_pressed = true
            color_button.grab_focus()
        else:
            color_button.text = ""


func select_color_for_button(button, html_color):
    for color_button in grid_colors.get_children():
        if color_button.get_theme_stylebox("normal").bg_color.to_html() == html_color:
            button.add_theme_stylebox_override("normal", color_button.get_theme_stylebox("normal"))
            button.add_theme_stylebox_override("hover", color_button.get_theme_stylebox("hover"))
            button.add_theme_stylebox_override("pressed", color_button.get_theme_stylebox("pressed"))
            break


func enable_player_nodes(boolean, nodes):
    for node in nodes:
        if node is Label or node is LineEdit:
            node.modulate = Color(1, 1, 1, 1) if boolean else Color(1, 1, 1, 0.5)
            if node is LineEdit:
                node.editable = boolean
        if node is Button:
            node.disabled = not(boolean)
            if node is CheckButton:
                node.button_pressed = boolean
            if node.disabled and node is CheckBox:
                node.set_pressed_no_signal(false)
            if node.name.to_snake_case().begins_with("color"):
                if str(global.client_index + 1) == node.name.to_snake_case()[-1] and boolean:
                    node.mouse_filter = MOUSE_FILTER_STOP
                else:
                    node.mouse_filter = MOUSE_FILTER_IGNORE


func check_player_ready(ready_player, player_name, player_color):
    var error_text = ""
    error_text = check_player_data(player_name, player_color)
    
    if error_text:
        change_label_network(error_text, true)
        ready_player.disabled = true
        ready_player.button_pressed = false
    else:
        ready_player.disabled = false
        if not ready_player.button_pressed:
            return change_label_network(message_ready, false)
        if global.host:
            error_text = check_vocabulary()
            if error_text:
                enable_button_apply(error_text, true)
            else:
                var message = check_all_players_ready()
                enable_button_apply(message, false)

    
func check_player_data(player_name, player_color):
    if player_name == "":
        return message_no_name
    
    var player_names = []
    var player_colors = []
    if global.client_index != 0 and check_player_1.button_pressed:
        if line_player_1.text:
            player_names.append(line_player_1.text)
        player_colors.append(color_player_1.get_theme_stylebox("normal").bg_color.to_html())
    if global.client_index != 1 and check_player_2.button_pressed:
        if line_player_2.text:
            player_names.append(line_player_2.text)
        player_colors.append(color_player_2.get_theme_stylebox("normal").bg_color.to_html())
    if global.client_index != 2 and check_player_3.button_pressed:
        if line_player_3.text:
            player_names.append(line_player_3.text)
        player_colors.append(color_player_3.get_theme_stylebox("normal").bg_color.to_html())
    if global.client_index != 3 and check_player_4.button_pressed:
        if line_player_4.text:
            player_names.append(line_player_4.text)
        player_colors.append(color_player_4.get_theme_stylebox("normal").bg_color.to_html())
    if player_name in player_names:
        return message_same_name
    if player_color.to_html() in player_colors:
        return message_same_color
    
    return ""


func check_vocabulary():
    if flow_units.get_children() == []:
        return tr("Please load a vocabulary list!")
    
    var unit_selected = false
    for child in flow_units.get_children():
        if child.button_pressed:
            unit_selected = true
            break
    if not unit_selected:
        return tr("Please choose one unit at least!")
    
    return ""


func check_all_players_ready():
    var player_number = [check_player_1.button_pressed, check_player_2.button_pressed,
            check_player_3.button_pressed, check_player_4.button_pressed]
    if player_number.count(true) == 1:
        return message_no_players
    if check_player_1.button_pressed:
        if not ready_player_1.button_pressed:
            return message_ready_all
    if check_player_2.button_pressed:
        if not ready_player_2.button_pressed:
            return message_ready_all
    if check_player_3.button_pressed:
        if not ready_player_3.button_pressed:
            return message_ready_all
    if check_player_4.button_pressed:
        if not ready_player_4.button_pressed:
            return message_ready_all
    return ""


func change_label_network(text, warning):
    label_network.text = text
    if warning:
        icon_warning.show()
    else:
        icon_warning.hide()


func enable_button_apply(message, warning):
    if message:
        button_apply.disabled = true
        button_apply.get_child(0).modulate = Color(1, 1, 1, 0.5)
        change_label_network(message, warning)
    else:
        button_apply.disabled = false
        button_apply.get_child(0).modulate = Color(1, 1, 1, 1)
        if global.host:
            change_label_network(message_start_server, warning)
        else:
            change_label_network(message_start_client, warning)


func show_message_dialog(message_text):
    message_dialog.label_title.text = tr("Error!")
    message_dialog.label_main.text = message_text
    if message_text == tr("The lobby was closed by the host."):
        message_dialog.texture_dialog.color = Color(0.125, 0.125, 0.125, 1)
        message_dialog.button_back.hide()
        message_dialog.button_close.show()
        message_dialog.label_close.text = tr("Main Menu")
    else:
        message_dialog.texture_dialog.color = Color(0.3, 0, 0, 1)
        message_dialog.button_back.show()
        message_dialog.button_close.hide()
    message_dialog.popup_centered()


func message_dialog_close_pressed():
    message_dialog.hide()
    global.clients.erase(global.client_index)
    global.peer.close()
    
    next_scene = "res://scenes/main.tscn"
    animation_setup.play("fade_out")


func send_packet(receivers, topic, data):
    if global.host and receivers == [0]:
        return
    var packet = {"receivers": receivers, "topic": topic, "data": data}
    global.peer.send_text(JSON.stringify(packet, "  "))


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]
    
    if topic == "client_connected":
        global.clients.append(int(data[0]))
        if label_network.text == message_start_server or label_network.text == message_start_client:
            enable_button_apply(message_ready_all, false)
        return
    
    if topic == "client_disconnected":
        var client_index = int(data[0])
        global.clients.erase(client_index)
        if client_index == 0:
            var text = tr("The lobby was closed by the host.")
            show_message_dialog(text)
            return
        if client_index == 1:
            line_player_2.text = ""
            enable_player_nodes(false, [check_player_2, ready_player_2, label_player_2,
                line_player_2, color_player_2])
        if client_index == 2:
            line_player_3.text = ""
            enable_player_nodes(false, [check_player_3, ready_player_3, label_player_3,
                line_player_3, color_player_3])
        if client_index == 3:
            line_player_4.text = ""
            enable_player_nodes(false, [check_player_4, ready_player_4, label_player_4,
                line_player_4, color_player_4])
        if not icon_warning.visible:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
        return
    
    if topic == "get_host_data":
        if label_network.text == message_no_players:
            label_network.text = message_ready_all
        
        send_packet(data, "label_file", label_file.text)
        send_packet(data, "button_card_number", str(global.card_number))
        send_packet(data, "timer_mode", str(global.timer_mode))
        send_packet(data, "unit_names", "\t".join(global.unit_names))
        for button in flow_units.get_children():
            var boolean = "true" if button.button_pressed else "false"
            var unit_string = str(button.get_index()) + "\t" + boolean
            send_packet(data, "unit_selected", unit_string)
        
        send_packet(data, "check_player_1", "true")
        send_packet(data, "line_player_1", global.player1.name)
        send_packet(data, "color_player_1", global.player1.color.to_html())
        if ready_player_1.button_pressed:
            send_packet(data, "ready_player_1", "true")
        
        if global.client_index != 1:
            if check_player_2.button_pressed:
                send_packet(data, "check_player_2", "true")
            send_packet(data, "line_player_2", global.player2.name)
            send_packet(data, "color_player_2", global.player2.color.to_html())
            if ready_player_2.button_pressed:
                send_packet(data, "ready_player_2", "true")
        if global.client_index != 2:
            if check_player_3.button_pressed:
                send_packet(data, "check_player_3", "true")
            send_packet(data, "line_player_3", global.player3.name)
            send_packet(data, "color_player_3", global.player3.color.to_html())
            if ready_player_3.button_pressed:
                send_packet(data, "ready_player_3", "true")
        if global.client_index != 3:
            if check_player_4.button_pressed:
                send_packet(data, "check_player_4", "true")
            send_packet(data, "line_player_4", global.player4.name)
            send_packet(data, "color_player_4", global.player4.color.to_html())
            if ready_player_4.button_pressed:
                send_packet(data, "ready_player_4", "true")
        
        return
    
    if topic == "host_returned":
        label_network.text = message_ready_all
        if global.client_index == 1:
            send_packet(global.clients, "check_player_2", "true")
            send_packet(global.clients, "line_player_2", line_player_2.text)
            var color_2 = color_player_2.get_theme_stylebox("normal").bg_color
            send_packet(global.clients, "color_player_2", color_2.to_html())
            var boolean_2 = "true" if ready_player_2.button_pressed else "false"
            send_packet(global.clients, "ready_player_2", boolean_2)
        if global.client_index == 2:
            send_packet(global.clients, "check_player_3", "true")
            send_packet(global.clients, "line_player_3", line_player_3.text)
            var color_3 = color_player_3.get_theme_stylebox("normal").bg_color
            send_packet(global.clients, "color_player_3", color_3.to_html())
            var boolean_3 = "true" if ready_player_3.button_pressed else "false"
            send_packet(global.clients, "ready_player_3", boolean_3)
        if global.client_index == 3:
            send_packet(global.clients, "check_player_4", "true")
            send_packet(global.clients, "line_player_4", line_player_4.text)
            var color_4 = color_player_4.get_theme_stylebox("normal").bg_color
            send_packet(global.clients, "color_player_4", color_4.to_html())
            var boolean_4 = "true" if ready_player_4.button_pressed else "false"
            send_packet(global.clients, "ready_player_4", boolean_4)
        return
    
    if topic == "label_file":
        label_file.text = data
        return
    
    if topic == "button_card_number":
        global.card_number = int(data)
        for button in hbox_cards.get_children():
            if data == button.get_child(0).text:
                button.disabled = false
                button.button_pressed = true
                button.mouse_filter = MOUSE_FILTER_IGNORE
            else:
                button.disabled = true
        return
    
    if topic == "timer_mode":
        global.timer_mode = int(data)
        for button in hbox_timer.get_children():
            if int(data) == button.get_index():
                button.disabled = false
                button.button_pressed = true
                button.mouse_filter = MOUSE_FILTER_IGNORE
            else:
                button.disabled = true
        return
    
    if topic == "unit_names":
        global.unit_names.clear()
        for unit in data.split("\t"):
            global.unit_names.append(unit)
        add_unit_buttons()
        return
    
    if topic == "unit_selected":
        var unit_data = data.split("\t")
        flow_units.get_children()[int(unit_data[0])].button_pressed = unit_data[1].contains("true")
        return
    
    if topic == "check_player_1":
        check_player_1.button_pressed = data.contains("true")
        if data.contains("true"):
            line_player_1.text = global.player1.name
            enable_player_nodes(true, [color_player_1])
        else:
            global.player1.name = ""
            enable_player_nodes(false, [check_player_1, ready_player_1, label_player_1,
                line_player_1, color_player_1])
        return
    
    if topic == "check_player_2":
        check_player_2.button_pressed = data.contains("true")
        if data.contains("true"):
            line_player_2.text = global.player2.name
            enable_player_nodes(true, [color_player_2])
        else:
            global.player2.name = ""
            enable_player_nodes(false, [check_player_2, ready_player_2, label_player_2,
                line_player_2, color_player_2])
        return
    
    if topic == "check_player_3":
        check_player_3.button_pressed = data.contains("true")
        if data.contains("true"):
            line_player_3.text = global.player3.name
            enable_player_nodes(true, [color_player_3])
        else:
            global.player3.name = ""
            enable_player_nodes(false, [check_player_3, ready_player_3, label_player_3,
                line_player_3, color_player_3])
        return
    
    if topic == "check_player_4":
        check_player_4.button_pressed = data.contains("true")
        if data.contains("true"):
            line_player_4.text = global.player4.name
            enable_player_nodes(true, [color_player_4])
        else:
            global.player4.name = ""
            enable_player_nodes(false, [check_player_4, ready_player_4, label_player_4,
                line_player_4, color_player_4])
        return
    
    if topic == "line_player_1":
        global.player1.name = data
        line_player_1.text = data
        check_double_names_or_colors()
        return
    
    if topic == "line_player_2":
        global.player2.name = data
        line_player_2.text = data
        check_double_names_or_colors()
        return
    
    if topic == "line_player_3":
        global.player3.name = data
        line_player_3.text = data
        check_double_names_or_colors()
        return
    
    if topic == "line_player_4":
        global.player4.name = data
        line_player_4.text = data
        check_double_names_or_colors()
        return

    if topic == "color_player_1":
        global.player1.color = Color(data)
        select_color_for_button(color_player_1, data)
        check_double_names_or_colors()
        return

    if topic == "color_player_2":
        global.player2.color = Color(data)
        select_color_for_button(color_player_2, data)
        check_double_names_or_colors()
        return
    
    if topic == "color_player_3":
        global.player3.color = Color(data)
        select_color_for_button(color_player_3, data)
        check_double_names_or_colors()
        return
    
    if topic == "color_player_4":
        global.player4.color = Color(data)
        select_color_for_button(color_player_4, data)
        check_double_names_or_colors()
        return
    
    if topic == "ready_player_1":
        ready_player_1.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
            return

    if topic == "ready_player_2":
        ready_player_2.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
            return

    if topic == "ready_player_3":
        ready_player_3.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
            return

    if topic == "ready_player_4":
        ready_player_4.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            enable_button_apply(message, false)
            return
    
    if topic == "game_started" and data == "true":
        next_scene = "res://scenes/game_online.tscn"
        animation_setup.play("fade_out")
        return


func check_double_names_or_colors():
    if global.client_index == 0:
        check_player_ready(ready_player_1, global.player1.name, global.player1.color)
    elif global.client_index == 1:
        check_player_ready(ready_player_2, global.player2.name, global.player2.color)
    elif global.client_index == 2:
        check_player_ready(ready_player_3, global.player3.name, global.player3.color)
    elif global.client_index == 3:
        check_player_ready(ready_player_4, global.player4.name, global.player4.color)


func host_returned():
    label_network.text = message_ready_all
    
    send_packet(global.clients, "button_card_number", str(global.card_number))
    send_packet(global.clients, "timer_mode", str(global.timer_mode))
    for button in flow_units.get_children():
        var boolean = "true" if button.button_pressed else "false"
        var unit_string = str(button.get_index()) + "\t" + boolean
        send_packet(global.clients, "unit_selected", unit_string)
    
    send_packet(global.clients, "check_player_1", "true")
    send_packet(global.clients, "line_player_1", global.player1.name)
    send_packet(global.clients, "color_player_1", global.player1.color.to_html())
    send_packet(global.clients, "host_returned", "")
