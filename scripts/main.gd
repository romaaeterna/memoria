class_name Main
extends Control


var next_scene = ""

@onready var margin_board = $VBoxGame/TextureBoard/MarginBoard
@onready var hbox_title = margin_board.get_node("VBoxBoard/HBoxTitle")
@onready var vbox_buttons = margin_board.get_node("VBoxBoard/VBoxButtons")
@onready var hbox_about = margin_board.get_node("VBoxBoard/HBoxAbout")

@onready var margin_bottom = $VBoxGame/MarginBottom
@onready var label_version = margin_bottom.get_node("HBoxBottom/LabelVersion")

@onready var animation_main = $AnimationMain
@onready var animation_start = $AnimationStart


func _on_ready():
    label_version.text = tr("Version: %s") % ProjectSettings.get_setting("application/config/version")
    if global.app_start:
        vbox_buttons.modulate = Color(1, 1, 1, 0)
        hbox_about.modulate = Color(1, 1, 1, 0)
        margin_bottom.modulate = Color(1, 1, 1, 0)
        for panel in hbox_title.get_children():
            panel.pivot_offset.x = 114
            panel.get_child(0).hide()
        for button in vbox_buttons.get_children():
            button.disabled = true
        for button in hbox_about.get_children():
            button.disabled = true
        animation_start.play("flip")
    else:
        margin_board.modulate = Color(1, 1, 1, 0)
        margin_bottom.modulate = Color(1, 1, 1, 0)
        animation_main.play("fade_in")


func _on_animation_start_finished(animation_name):
    if animation_name == "flip":
        global.app_start = false
        animation_start.play("start_game")
    if animation_name == "start_game":
        for button in vbox_buttons.get_children():
            button.disabled = false
        for button in hbox_about.get_children():
            button.disabled = false


func _on_animation_main_finished(animation_name):
    if animation_name == "fade_out":
        get_tree().change_scene_to_file(next_scene)
    if animation_name == "quit_game":
        get_tree().quit()


func _on_button_new_game_pressed():
    next_scene = "res://scenes/setup.tscn"
    animation_main.play("fade_out")


func _on_button_high_score_pressed():
    next_scene = "res://scenes/high_score.tscn"
    animation_main.play("fade_out")


func _on_button_editor_pressed():
    next_scene = "res://scenes/editor.tscn"
    animation_main.play("fade_out")


func _on_button_settings_pressed():
    global.last_scene = "main"
    next_scene = "res://scenes/settings.tscn"
    animation_main.play("fade_out")


func _on_button_documentation_pressed():
    OS.shell_open("https://memoria2.readthedocs.io/en/latest/")


func _on_button_about_pressed():
    next_scene = "res://scenes/about.tscn"
    animation_main.play("fade_out")


func _on_button_quit_pressed():
    animation_main.play("quit_game")
