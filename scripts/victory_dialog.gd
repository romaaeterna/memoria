class_name VictoryDialog
extends Window


var stylebox_label = load("res://tres/style_label_winner.tres")
var texture_drawn = load("res://assets/textures/drawn.svg")
var texture_victory = load("res://assets/textures/victory.svg")

@onready var margin_content = $MarginVictory/TextureVictory/MarginContent
@onready var label_title = margin_content.get_node("CenterTitle/LabelTitle")
@onready var vbox_content = margin_content.get_node("VBoxContent")
@onready var hbox_winner = vbox_content.get_node("MarginWinner/HBoxWinner")
@onready var label_text = hbox_winner.get_node("VBoxWinner/LabelText")
@onready var label_winner = hbox_winner.get_node("VBoxWinner/LabelWinner")
@onready var texture_winner = hbox_winner.get_node("TextureWinner")

@onready var hbox_buttons = vbox_content.get_node("HBoxButtons")
@onready var button_main = hbox_buttons.get_node("ButtonMain")
@onready var button_score = hbox_buttons.get_node("ButtonScore")


func _on_ready():
    if global.winner:
        label_title.text = tr("Congratulations!")
        label_text.text = tr("The winner is:")
        texture_winner.texture = texture_victory
        label_winner.text = global.winner.name
        stylebox_label.bg_color = global.winner.color
        if global.winner.color.get_luminance() < 0.5:
            label_winner.add_theme_color_override("font_color", Color(1, 1, 1, 1))
        else:
            label_winner.add_theme_color_override("font_color", Color(0, 0, 0, 1))
        label_winner.show()
        if get_parent().name == "GameLocal":
            global.load_high_score()
            global.update_high_score()
    else:
        label_title.text = tr("Game Finished!")
        label_text.text = tr("The game ended\nin a drawn.")
        texture_winner.texture = texture_drawn
        label_winner.hide()


func _on_button_close_pressed():
    self.hide()
