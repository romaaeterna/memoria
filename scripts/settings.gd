class_name Settings
extends Control


@onready var margin_top = $VBoxSettings/MarginTop

@onready var margin_board = $VBoxSettings/TextureBoard/MarginBoard
@onready var hbox_board = margin_board.get_node("PanelBoard/MarginBoard/HBoxBoard")
@onready var grid_general = hbox_board.get_node("GridGeneral")
@onready var button_english = grid_general.get_node("HBoxLanguage/ButtonEnglish")
@onready var button_german = grid_general.get_node("HBoxLanguage/ButtonGerman")
@onready var slider_sounds = grid_general.get_node("HBoxSounds/SliderSounds")
@onready var label_volume = grid_general.get_node("HBoxSounds/LabelVolume")
@onready var label_timer = grid_general.get_node("LabelTimer")
@onready var hbox_timer = grid_general.get_node("HBoxTimer")
@onready var slider_timer = hbox_timer.get_node("SliderTimer")
@onready var label_length = hbox_timer.get_node("LabelLength")

@onready var grid_vocables = hbox_board.get_node("GridVocables")
@onready var label_default_dir = grid_vocables.get_node("LabelDefaultDir")
@onready var hbox_default_dir = grid_vocables.get_node("HBoxDefaultDir")
@onready var text_default_dir = hbox_default_dir.get_node("ButtonDefaultDir/TextDefaultDir")
@onready var label_default_file = grid_vocables.get_node("LabelDefaultFile")
@onready var hbox_default_file = grid_vocables.get_node("HBoxDefaultFile")
@onready var text_default_file = hbox_default_file.get_node("ButtonDefaultFile/TextDefaultFile")
@onready var hbox_default_cards = grid_vocables.get_node("HBoxDefaultCards")

@onready var animation_settings = $AnimationSettings


func _on_ready():
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    animation_settings.play("fade_in")
    
    hide_nodes()
    apply_settings()


func _on_animation_settings_finished(animation_name):
    if animation_name == "fade_out":
        if global.last_scene == "game_local":
            var game_local = get_tree().get_root().get_node("GameLocal")
            game_local.get_node("PopupSettings").hide()
        elif global.last_scene == "game_online":
            var game_online = get_tree().get_root().get_node("GameOnline")
            game_online.get_node("PopupSettings").hide()
        else:
            var next_scene = "res://scenes/main.tscn"
            get_tree().change_scene_to_file(next_scene)


func _on_button_close_pressed():
    save_settings()
    animation_settings.play("fade_out")


func _on_button_language_pressed(language_code):
    global.language = language_code
    TranslationServer.set_locale(language_code)


func _on_slider_sounds_value_changed(value):
    global.sounds = value
    label_volume.text = "%s %%" % str(value * 100)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(value))


func _on_slider_timer_value_changed(value):
    global.countdown_length = value
    label_length.text = "%s s" % str(value)


func _on_button_default_dir_pressed():
    var dialog_title = tr("Select a Directory")
    var dialog_dir = global.default_dir if global.default_dir else global.documents_dir
    var dialog_mode = DisplayServer.FILE_DIALOG_MODE_OPEN_DIR
    var dialog_filter = PackedStringArray([])
    DisplayServer.file_dialog_show(dialog_title, dialog_dir, "", false, dialog_mode, dialog_filter,
        _on_native_file_dialog_dir_selected)


func _on_native_file_dialog_dir_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    global.default_dir = selected_paths[0]
    text_default_dir.text = selected_paths[0].replace("\\", "/").split("/")[-1]


func _on_button_default_dir_revert_pressed():
    global.default_dir = ""
    text_default_dir.text = tr("None")


func _on_button_default_file_pressed():
    var dialog_title = tr("Select a File")
    var dialog_dir = global.default_dir if global.default_dir else global.documents_dir
    var dialog_mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    var dialog_filter = PackedStringArray(["*.csv ; CSV Files"])
    DisplayServer.file_dialog_show(dialog_title, dialog_dir, "", false, dialog_mode, dialog_filter,
        _on_native_file_dialog_file_selected)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    global.default_file = selected_paths[0]
    text_default_file.text = selected_paths[0].get_file()


func _on_button_default_file_revert_pressed():
    global.default_file = ""
    text_default_file.text = tr("None")


func _on_button_card_number_pressed(number):
    global.default_card_number = number


func hide_nodes():
    if OS.get_name() == "Web":
        label_default_dir.hide()
        hbox_default_dir.hide()
        label_default_file.hide()
        hbox_default_file.hide()
    if global.last_scene == "game_online":
        label_timer.hide()
        hbox_timer.hide()


func apply_settings():
    toggle_language_button()
    slider_sounds.value = global.sounds
    slider_timer.value = global.countdown_length
    set_default_dir_and_file()
    set_default_card_number()


func toggle_language_button():
    if global.language == "de":
        button_german.button_pressed = true
    elif global.language == "en":
        button_english.button_pressed = true
    TranslationServer.set_locale(global.language)


func set_default_dir_and_file():
    if global.default_dir == "":
        text_default_dir.text = tr("None")
    else:
        var folder = global.default_dir.replace("\\", "/").split("/")[-1]
        text_default_dir.text = folder
    if global.default_file == "":
        text_default_file.text = tr("None")
    else:
        var file = global.default_file.get_file()
        text_default_file.text = file


func set_default_card_number():
    for button in hbox_default_cards.get_children():
        if button.get_child(0).text == str(global.default_card_number):
            button.button_pressed = true
            break


func save_settings():
    var config = ConfigFile.new()
    config.set_value("General", "countdown", global.countdown_length)
    config.set_value("General", "language", global.language)
    config.set_value("General", "sounds", global.sounds)
    config.set_value("Vocables", "default_card_number", global.default_card_number)
    config.set_value("Vocables", "default_dir", global.default_dir)
    config.set_value("Vocables", "default_file", global.default_file)
    config.save("user://settings.ini")
