class_name Setup
extends Control


const url = "wss://romaaeterna.fly.dev/"
# const url = "ws://localhost:9080"         # for local testing

var connected = false
var init_lobby = false
var lobby_name = ""
var next_scene = ""

var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

@onready var margin_top = $VBoxSetup/MarginTop
@onready var margin_board = $VBoxSetup/TextureBoard/MarginBoard
@onready var vbox_network = margin_board.get_node("HBoxBoard/PanelNetwork/MarginNetwork/VBoxNetwork")
@onready var line_lobby = vbox_network.get_node("HBoxLobby/LineLobby")
@onready var margin_bottom = $VBoxSetup/MarginBottom

@onready var animation_setup = $AnimationSetup
@onready var dialog_connect = $DialogConnect
@onready var timer_connect = $DialogConnect/TimerConnect
@onready var vbox_connect = $DialogConnect/MarginConnect/TextureConnect/MarginContent/VBoxContent
@onready var sprite_connect = vbox_connect.get_node("CenterConnect/SpriteConnect")


func _on_ready():
    add_child(message_dialog)
    
    margin_top.modulate = Color(1, 1, 1, 0)
    margin_board.modulate = Color(1, 1, 1, 0)
    margin_bottom.modulate = Color(1, 1, 1, 0)
    animation_setup.play("fade_in")
    
    global.peer = WebSocketPeer.new()


func _process(_delta):
    if connected:
        global.peer.poll()
        var state = global.peer.get_ready_state()
        if state == WebSocketPeer.STATE_OPEN:
            if init_lobby:
                var packet = {"lobby_game": "memoria", "lobby_host": global.host,
                    "lobby_name": lobby_name}
                global.peer.send_text(JSON.stringify(packet, "  "))
                init_lobby = false
            while global.peer.get_available_packet_count():
                packet_received(global.peer.get_packet())
        if state == WebSocketPeer.STATE_CLOSED:
            connected = false


func _on_tree_exiting():
    message_dialog.queue_free()


func _on_setup_animation_finished(animation_name):
    if animation_name == "fade_out":
        get_tree().change_scene_to_file(next_scene)


func _on_button_close_pressed():
    global.peer.close()
    next_scene = "res://scenes/main.tscn"
    animation_setup.play("fade_out")


func _on_button_local_pressed():
    next_scene = "res://scenes/setup_local.tscn"
    animation_setup.play("fade_out")


func _on_line_lobby_text_submitted(_text):
    _on_button_join_lobby_pressed()


func _on_button_create_lobby_pressed():
    lobby_name = line_lobby.text.to_lower()
    if lobby_name == "":
        var text = tr("Please type in the name of the lobby you want to create!")
        return show_message_dialog(text)
    
    dialog_connect.show()
    sprite_connect.play("spinning")
    timer_connect.start()
    
    global.peer.connect_to_url(url)
    global.host = true
    connected = true
    init_lobby = true


func _on_button_join_lobby_pressed():
    lobby_name = line_lobby.text.to_lower()
    if lobby_name == "":
        var text = tr("Please type in the name of the lobby you want to join!")
        return show_message_dialog(text)
    
    dialog_connect.show()
    sprite_connect.play("spinning")
    timer_connect.start()
    
    global.peer.connect_to_url(url)
    global.host = false
    connected = true
    init_lobby = true


func show_message_dialog(message_text):
    message_dialog.texture_dialog.color = Color(0.3, 0, 0, 1)
    message_dialog.label_title.text = tr("Error!")
    message_dialog.label_main.text = message_text
    message_dialog.button_close.hide()
    message_dialog.popup_centered()


func _on_timer_connect_timeout():
    global.peer.close()
    dialog_connect.hide()
    var text = tr("Cannot connect to server!")
    show_message_dialog(text)


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]
    
    if topic == "client_connected":
        global.client_index = int(data[0])
        global.clients = data[1].map(func(x): return int(x))
        next_scene = "res://scenes/setup_online.tscn"
        animation_setup.play("fade_out")
        dialog_connect.hide()
        timer_connect.stop()
        set_process(false)
        return

    var text = ""
    if topic in ["lobby_exists", "lobby_full", "lobby_not_found", "lobby_max_reached",
        "game_already_started"]:
        global.peer.close()
        if topic == "lobby_exists":
            text = tr("The lobby you want to create already exists.")
        if topic == "lobby_full":
            text = tr("There are already four players in the lobby.")
        if topic == "lobby_not_found":
            text = tr("There is no lobby with the given name.")
        if topic == "lobby_max_reached":
            text = tr("The maximum number of lobbys is reached.")
        if topic == "game_already_started":
            text = tr("The game you want to join has already started.")
            
        dialog_connect.hide()
        timer_connect.stop()
        show_message_dialog(text)
