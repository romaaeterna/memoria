# Memoria

## Introduction

Memoria is a simple application to play Memory (Matching) for up to four players.

You can download the game from the [Itch.io website](https://romaaeterna.itch.io/memoria).

See the [online documentation](https://memoria2.readthedocs.io/en/latest/) for instructions on how to play.

## Source code & License

The source code of Memoria is hosted on [GitLab](https://gitlab.com/romaaeterna/memoria).

The game is released under the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0).
See the file [LICENSE.txt](LICENSE.txt) for more information.

## Special thanks to

- the developers of the [Godot Engine](https://godotengine.org/)
- the Latin students of the [Vicco-von-Bülow-Gymnasium Falkensee](http://www.vicco-von-buelow-gymnasium-falkensee.de/) (for being enthusiastic beta testers)
