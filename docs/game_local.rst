Local Game
==========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Click the button "New Game" to open the setup screen.

.. image:: images/screen_main.png

Select "Local Game" on the left-hand side.

.. image:: images/screen_setup.png

First click on the button at the top left and navigate to the file you want to open.
Now you can choose the units you want your students to repeat.

.. image:: images/screen_setup_local_1.png

Memoria can be played by up to four students or teams. Select the number of players by clicking on the corresponding check boxes.
Type in the name of each player and change the color if you want.
Finally select the number of vocable cards and press the button "Start Game".

.. image:: images/screen_setup_local_2.png


Playing the game
----------------

You'll find the name of the active player in the header bar, the score and statistics at the bottom of the window.

.. image:: images/screen_game_local_1.png

Try to find a vocable and its translation by clicking on two white rectangels. If you found a matching pair, the rectangels are highlighted by the player's color and you can go on. Otherwise it's the turn of the next player.

.. image:: images/screen_game_local_2.png

The player who found most matching pairs wins the game.

.. image:: images/screen_game_local_3.png
