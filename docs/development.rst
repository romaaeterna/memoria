Development
===========

The current version of Memoria is 2.0.0.


Bugs
----

Known Bugs
~~~~~~~~~~

At the moment no bugs are known.

Filing A Bug
~~~~~~~~~~~~

If you’ve found a bug in Memoria, please head over to GitLab and `file a report <https://gitlab.com/romaaeterna/memoria/issues>`_.
Filing bugs helps improve the software for everyone.


Source Code
-----------

The full source code of Memoria is hosted on `GitLab <https://gitlab.com/romaaeterna/memoria>`_.
