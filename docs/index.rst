Memoria
=======

.. image:: images/logo.png
    :align: center

****

**Memoria is a simple application to play Memory (Matching) for up to four players.**

The program is released under the `GNU General Public License (GPL) version 3 <http://www.gnu.org/licenses/>`_.


Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   setup
   game_local
   game_online
   development
   credits
