Getting started
===============


Vocabulary list
---------------

To play Memoria you need a vocabulary list.
You can use the build-in vocabulary editor, which features easy and intuitive operation, to create new lists or edit existing ones.

.. image:: images/screen_editor.png

Alternatively, you can create and edit vocabulary lists as CSV files by your own.
Please note that the application can only load files with the following data structure::

   [unit],[vocable],[translation]

Your vocabulary list should look like this example (separated by comma, semicolon or tab)::

   1,this,dies
   1,is,ist
   2,an,ein
   2,example,Beispiel


Settings
--------

There are several general settings you may adapt to your needs.

.. image:: images/screen_settings.png
