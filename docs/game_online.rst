Online Game
===========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Click the button "New Game" to open the setup screen.

.. image:: images/screen_main.png

Type in the name of the lobby you want to either create or join. The online game mode can be played by 2 to 4 players.

.. image:: images/screen_setup.png

Once you've created the lobby, you'll need to set up the game as a host:

1. Click the button at the top left and navigate to the file you want to open. Then choose the units you want to repeat.
2. Set the number of cards and the timer duration. 
3. Enter your name, select your color and click the "Ready" button.
4. Click the button "Start Game" when all players are ready.

.. image:: images/screen_setup_online_1.png

If you've joined the lobby, all you need to do is enter your name, select your color and click the "Ready" button.

.. image:: images/screen_setup_online_2.png


Playing the game
----------------

The online game is turn-based. You'll find the name of the active player in the header bar, the score and statistics at the bottom of the window.

.. image:: images/screen_game_online_1.png

Try to find a vocable and its translation by clicking on two white rectangels. You only have a limited amount of time which is shown above.
If you found a matching pair, the rectangels are highlighted by the player's color and you can go on. Otherwise it's the turn of the next player.

.. image:: images/screen_game_online_2.png

The player who found most matching pairs wins the game.

.. image:: images/screen_game_online_3.png
