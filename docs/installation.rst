Installation
============


Microsoft Windows
-----------------

To install Memoria follow the setup wizard of `MemoriaSetup.exe <https://romaaeterna.itch.io/memoria>`_.

If you don't want to install the game, just unzip `MemoriaWindows.zip <https://romaaeterna.itch.io/memoria>`_. and execute the file ``Memoria.exe``.


Linux
-----

To play Memoria just unzip `MemoriaLinux.zip <https://romaaeterna.itch.io/memoria>`_. and execute the file ``Memoria.sh``.


Build with Godot Engine
-----------------------

If you want to build the game on your own, modify or improve it, feel free to do so.
All you need is the `Godot Engine <https://godotengine.org/>`_ (>= 4.3) and the `source code hosted on GitLab <https://gitlab.com/romaaeterna/memoria>`_.
