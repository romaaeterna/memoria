# LANGUAGE translation for Memoria for the following files:
# res://scenes/about.tscn
# res://scenes/editor.tscn
# res://scenes/game_local.tscn
# res://scenes/game_online.tscn
# res://scenes/high_score.tscn
# res://scenes/main.tscn
# res://scenes/message_dialog.tscn
# res://scenes/settings.tscn
# res://scenes/setup.tscn
# res://scenes/setup_local.tscn
# res://scenes/setup_online.tscn
# res://scenes/victory_dialog.tscn
# res://scripts/about.gd
# res://scripts/editor.gd
# res://scripts/game_local.gd
# res://scripts/game_online.gd
# res://scripts/high_score.gd
# res://scripts/main.gd
# res://scripts/message_dialog.gd
# res://scripts/settings.gd
# res://scripts/setup.gd
# res://scripts/setup_local.gd
# res://scripts/setup_online.gd
# res://scripts/victory_dialog.gd
#
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Memoria\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: scenes/editor.tscn
#: scenes/main.tscn
msgid "Vocable Editor"
msgstr ""

#: scenes/editor.tscn
msgid "New File"
msgstr ""

#: scenes/editor.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "Open File"
msgstr ""

#: scenes/editor.tscn
msgid "Save File"
msgstr ""

#: scenes/editor.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
#: scripts/editor.gd
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "No File Loaded"
msgstr ""

#: scenes/editor.tscn
msgid "Units"
msgstr ""

#: scenes/editor.tscn
msgid "Add Unit"
msgstr ""

#: scenes/editor.tscn
msgid "Remove Unit"
msgstr ""

#: scenes/editor.tscn
#: scenes/settings.tscn
msgid "Vocables"
msgstr ""

#: scenes/editor.tscn
#: scripts/editor.gd
msgid "Load a file or create a first unit."
msgstr ""

#: scenes/editor.tscn
#: scripts/editor.gd
msgid "Add"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
#: scenes/main.tscn
#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "New Game"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
#: scenes/main.tscn
#: scenes/settings.tscn
msgid "Settings"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
#: scenes/victory_dialog.tscn
#: scripts/game_local.gd
#: scripts/game_online.gd
#: scripts/setup_online.gd
msgid "Main Menu"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
#: scenes/main.tscn
msgid "Quit"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
msgid "Pairs found:"
msgstr ""

#: scenes/game_local.tscn
#: scenes/game_online.tscn
msgid "Tries:"
msgstr ""

#: scenes/high_score.tscn
#: scripts/high_score.gd
msgid "Delete High Score"
msgstr ""

#: scenes/high_score.tscn
#: scenes/main.tscn
#: scenes/victory_dialog.tscn
msgid "High Score"
msgstr ""

#: scenes/high_score.tscn
msgid "The high score is empty."
msgstr ""

#: scenes/main.tscn
msgid "Online Documentation"
msgstr ""

#: scenes/main.tscn
msgid "About Memoria"
msgstr ""

#: scenes/main.tscn
#: scripts/about.gd
msgid "© 2024 Roma Aeterna"
msgstr ""

#: scenes/message_dialog.tscn
msgid "Back"
msgstr ""

#: scenes/settings.tscn
msgid "General"
msgstr ""

#: scenes/settings.tscn
msgid "Language"
msgstr ""

#: scenes/settings.tscn
msgid "English"
msgstr ""

#: scenes/settings.tscn
msgid "German"
msgstr ""

#: scenes/settings.tscn
#: scripts/about.gd
msgid "Sounds"
msgstr ""

#: scenes/settings.tscn
msgid "Timer Length"
msgstr ""

#: scenes/settings.tscn
msgid "Default Directory"
msgstr ""

#: scenes/settings.tscn
#: scripts/settings.gd
msgid "None"
msgstr ""

#: scenes/settings.tscn
msgid "Revert"
msgstr ""

#: scenes/settings.tscn
msgid "Default File"
msgstr ""

#: scenes/settings.tscn
msgid "Default Card Number"
msgstr ""

#: scenes/settings.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "12"
msgstr ""

#: scenes/settings.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "16"
msgstr ""

#: scenes/settings.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "20"
msgstr ""

#: scenes/settings.tscn
#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "24"
msgstr ""

#: scenes/setup.tscn
msgid "Game Setup"
msgstr ""

#: scenes/setup.tscn
msgid "Local Game"
msgstr ""

#: scenes/setup.tscn
msgid ""
"Play the game in class:\n"
"• either in opposing teams of up to 16 players,\n"
"• or in an one-on-one match of 2 to 4 players."
msgstr ""

#: scenes/setup.tscn
msgid "Online Game"
msgstr ""

#: scenes/setup.tscn
msgid ""
"Play the game online with 2 to 4 players:\n"
"• one player is the host who creates the lobby and sets up the game,\n"
"• the other players join this lobby."
msgstr ""

#: scenes/setup.tscn
msgid "Lobby Name:"
msgstr ""

#: scenes/setup.tscn
msgid "Create Lobby"
msgstr ""

#: scenes/setup.tscn
msgid "Join Lobby"
msgstr ""

#: scenes/setup.tscn
msgid "Connecting to Server …"
msgstr ""

#: scenes/setup_local.tscn
msgid "Setup Local Game"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "Number of Cards:"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "PLAYER 1"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "Name:"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "PLAYER 2"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "PLAYER 3"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "PLAYER 4"
msgstr ""

#: scenes/setup_local.tscn
#: scenes/setup_online.tscn
msgid "Start Game"
msgstr ""

#: scenes/setup_local.tscn
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "Please load a vocabulary list!"
msgstr ""

#: scenes/setup_online.tscn
msgid "Setup Online Game"
msgstr ""

#: scenes/setup_online.tscn
msgid "Timer Duration:"
msgstr ""

#: scenes/setup_online.tscn
msgid "short"
msgstr ""

#: scenes/setup_online.tscn
msgid "normal"
msgstr ""

#: scenes/setup_online.tscn
msgid "long"
msgstr ""

#: scenes/setup_online.tscn
msgid "Ready"
msgstr ""

#: scripts/about.gd
msgid "Memoria"
msgstr ""

#: scripts/about.gd
msgid "Version"
msgstr ""

#: scripts/about.gd
msgid "Programming & Design"
msgstr ""

#: scripts/about.gd
msgid "Textures & Icons"
msgstr ""

#: scripts/about.gd
msgid "Fonts"
msgstr ""

#: scripts/about.gd
msgid "Licenses"
msgstr ""

#: scripts/about.gd
msgid "Made with Godot Engine 4"
msgstr ""

#: scripts/editor.gd
msgid "Source Language"
msgstr ""

#: scripts/editor.gd
msgid "Target Language"
msgstr ""

#: scripts/editor.gd
#: scripts/settings.gd
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "Select a File"
msgstr ""

#: scripts/editor.gd
msgid "Save a File"
msgstr ""

#: scripts/editor.gd
msgid "Rename"
msgstr ""

#: scripts/editor.gd
msgid "Type in some vocables for the current unit."
msgstr ""

#: scripts/editor.gd
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid ""
"Can't load vocabulary list!\n"
"The data structure is invalid."
msgstr ""

#: scripts/editor.gd
#: scripts/setup.gd
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "Error!"
msgstr ""

#: scripts/editor.gd
msgid "If you create a new vocabulary list all changes will be discarded!"
msgstr ""

#: scripts/editor.gd
msgid "Warning!"
msgstr ""

#: scripts/editor.gd
msgid "Create without Saving"
msgstr ""

#: scripts/editor.gd
msgid "If you open a new vocabulary list all changes will be discarded!"
msgstr ""

#: scripts/editor.gd
msgid "Open without Saving"
msgstr ""

#: scripts/editor.gd
#: scripts/game_local.gd
#: scripts/high_score.gd
#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid ""
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Next Player: %s s"
msgstr ""

#: scripts/game_local.gd
msgid "%s s"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Player: %s"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Abort Game?"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Do you want to start a new game?"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Do you want to return to the main menu?"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Quit Game?"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Do you want to quit the application?"
msgstr ""

#: scripts/game_local.gd
#: scripts/game_online.gd
msgid "Quit Game"
msgstr ""

#: scripts/game_local.gd
#: scripts/victory_dialog.gd
msgid "Game Finished!"
msgstr ""

#: scripts/game_online.gd
msgid "Game Start: %s s"
msgstr ""

#: scripts/game_online.gd
msgid "Time Left: %s s"
msgstr ""

#: scripts/game_online.gd
msgid "Player: You (%s)"
msgstr ""

#: scripts/game_online.gd
msgid "Rematch"
msgstr ""

#: scripts/game_online.gd
msgid "Game Over!"
msgstr ""

#: scripts/game_online.gd
msgid "The game was canceled by the host."
msgstr ""

#: scripts/game_online.gd
msgid "The last opponent left the game."
msgstr ""

#: scripts/high_score.gd
msgid "Pl."
msgstr ""

#: scripts/high_score.gd
msgid "Name"
msgstr ""

#: scripts/high_score.gd
msgid "Victories"
msgstr ""

#: scripts/high_score.gd
msgid "Delete High Score?"
msgstr ""

#: scripts/high_score.gd
msgid "Do you want to delete the entire high score?"
msgstr ""

#: scripts/main.gd
msgid "Version: %s"
msgstr ""

#: scripts/settings.gd
msgid "Select a Directory"
msgstr ""

#: scripts/setup.gd
msgid "Please type in the name of the lobby you want to create!"
msgstr ""

#: scripts/setup.gd
msgid "Please type in the name of the lobby you want to join!"
msgstr ""

#: scripts/setup.gd
msgid "Cannot connect to server!"
msgstr ""

#: scripts/setup.gd
msgid "The lobby you want to create already exists."
msgstr ""

#: scripts/setup.gd
msgid "There are already four players in the lobby."
msgstr ""

#: scripts/setup.gd
msgid "There is no lobby with the given name."
msgstr ""

#: scripts/setup.gd
msgid "The maximum number of lobbys is reached."
msgstr ""

#: scripts/setup.gd
msgid "The game you want to join has already started."
msgstr ""

#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid ""
"The number of vocables is too small.\n"
"Please select more units or decrease the number of cards."
msgstr ""

#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "Please choose one unit at least!"
msgstr ""

#: scripts/setup_local.gd
msgid "Please type in a name for each player!"
msgstr ""

#: scripts/setup_local.gd
msgid "Please type in different names for each player!"
msgstr ""

#: scripts/setup_local.gd
msgid "Please select different colors for each player!"
msgstr ""

#: scripts/setup_local.gd
#: scripts/setup_online.gd
msgid "X"
msgstr ""

#: scripts/setup_online.gd
msgid "Please type in your name!"
msgstr ""

#: scripts/setup_online.gd
msgid "Waiting for other players …"
msgstr ""

#: scripts/setup_online.gd
msgid "Click \"Ready\" when your are finished!"
msgstr ""

#: scripts/setup_online.gd
msgid "Waiting for all players to be ready …"
msgstr ""

#: scripts/setup_online.gd
msgid "Please select a different color!"
msgstr ""

#: scripts/setup_online.gd
msgid "Please type in a different name!"
msgstr ""

#: scripts/setup_online.gd
msgid "Click \"Start Game\"!"
msgstr ""

#: scripts/setup_online.gd
msgid "Waiting for game start by host …"
msgstr ""

#: scripts/setup_online.gd
msgid "The lobby was closed by the host."
msgstr ""

#: scripts/victory_dialog.gd
msgid "Congratulations!"
msgstr ""

#: scripts/victory_dialog.gd
msgid "The winner is:"
msgstr ""

#: scripts/victory_dialog.gd
msgid ""
"The game ended\n"
"in a drawn."
msgstr ""
